<?php get_header(); ?>

<main>

<section class="pageHeader relative">
	<div class="bgGrad pageHeaderText relative" data-aos="fade-right">
		<p class="pageHeaderEn fontEnBrush white">Staff</p>
		<h3 class="h2 bold white">スタッフ紹介</h3>
	</div>
	<div class="pageHeaderImgBox bgImg absolute" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_about_01.jpg')" data-aos="fade-left"></div>
	
</section>



<div class="pageMessageMes">
	<div class="container">
		<p class="fontEn h1 mainColor">Our Members</p>
		<h3 class="h3 bold subColor mb50">スタッフ紹介</h3>

		<?php get_template_part('parts/temp-staff'); ?>
	</div>
</div>

<section class="pageAbout2Sec">
	<div class="container-fluid">
		<div class="row mb50">
			<div class="col-sm-7" data-aos="fade-right">
				<div class="shadowBox relative">
					<img class="" src="<?php echo get_template_directory_uri();?>/img/page_staff_01.jpg" alt="">
					<div class="shadowBoxShadow absolute bgGrad"></div>
				</div>
			</div>
			<div class="col-sm-5" data-aos="fade-up">
				<div class="pageAbout2SecTextBox right">
					<h4 class="h2 bold subColor mb10">色んな人の思いが詰まった図面が建物として形に</h4>
					<div class="pageAboutWorkUl titleBd">
						<ul class="inline_block mb10">
							<li class="bold h4">布池　洋平</li>
							<li class="gray text_m">2013年　中途入社</li>
						</ul>
					</div>
					<p class="gray">大学では地震学を専攻後、不動産営業を経て、データ分析系のコンサルタント業に就く。間接的な業務支援よりも組織の内部に入って自ら事業を育てていくことに興味を持ち、クレストへ。クレストでの現場を主に担当。</p>
					<div class="absolute pageAbout2SecNum fontEnBrush topFvEn bgGradText">01</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="mb50">
			<dl class="pageHospDl mb30">
				<dt class="tra">
					<ul class="inline_block mb10">
						<li class="pageHospHr"><hr></li>
						<li class="bold pageHospTitle h4 subColor">09:00　出社</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inline_block">
						<li class="pageHospImg lh_l">朝礼後、営業ミーティング。1日の営業先を再度確認します。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="pageHospDl mb30">
				<dt class="tra">
					<ul class="inline_block mb10">
						<li class="pageHospHr"><hr></li>
						<li class="bold pageHospTitle h4 subColor">09:00　出社</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inline_block">
						<li class="pageHospImg lh_l">朝礼後、営業ミーティング。1日の営業先を再度確認します。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="pageHospDl mb30">
				<dt class="tra">
					<ul class="inline_block mb10">
						<li class="pageHospHr"><hr></li>
						<li class="bold pageHospTitle h4 subColor">09:00　出社</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inline_block">
						<li class="pageHospImg lh_l">朝礼後、営業ミーティング。1日の営業先を再度確認します。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="pageHospDl mb30">
				<dt class="tra">
					<ul class="inline_block mb10">
						<li class="pageHospHr"><hr></li>
						<li class="bold pageHospTitle h4 subColor">09:00　出社</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inline_block">
						<li class="pageHospImg lh_l">朝礼後、営業ミーティング。1日の営業先を再度確認します。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="pageHospDl mb30">
				<dt class="tra">
					<ul class="inline_block mb10">
						<li class="pageHospHr"><hr></li>
						<li class="bold pageHospTitle h4 subColor">09:00　出社</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inline_block">
						<li class="pageHospImg lh_l">朝礼後、営業ミーティング。1日の営業先を再度確認します。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="pageHospDl mb30">
				<dt class="tra">
					<ul class="inline_block mb10">
						<li class="pageHospHr"><hr></li>
						<li class="bold pageHospTitle h4 subColor">09:00　出社</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inline_block">
						<li class="pageHospImg lh_l">朝礼後、営業ミーティング。1日の営業先を再度確認します。</li>
					</ul>
				
				</dd>
			</dl>
		</div>
		<div class="row mb50" data-aos="fade-up">
			<div class="col-sm-6 col-sm-push-6">
				<img class="mb30" src="<?php echo get_template_directory_uri();?>/img/page_staff_02.jpg" alt="">
			</div>
			<div class="col-sm-6 col-sm-pull-6">
				<p class="grayColor bold">Q1.この仕事の魅力って一言で言うと何でしょうか？</p>
				<h4 class="mainColor  bold h3 mb30">建物に機能を与え、命を吹き込むこと</h4>
				<p class="grayColor">この仕事の魅力は、色んな人の思いが詰まった図面が次第に建物として形になっていくことでです。それを日々、間近で感じながら業務に取り組んでいます。やはり図面通りのものが予定内の工期で終わると達成感が大きく、日々成長できる環境に満足しています。クレストでは入社後の先輩方や同僚のフォローも手厚く働きやすい環境が整っていると思います。</p>
			</div>
		</div>
	</div>
</section>	
<section class="pageAbout2Sec bgWhite padding">
	<div class="container-fluid">
	
		<div class="row mb100">
			<div class="col-sm-7 col-sm-push-5" data-aos="fade-left">
				<div class="shadowBox relative">
					<img class="" src="<?php echo get_template_directory_uri();?>/img/page_staff_01.jpg" alt="">
					<div class="shadowBoxShadow absolute bgGrad"></div>
				</div>
			</div>
			<div class="col-sm-5 col-sm-pull-7" data-aos="fade-up">
				<div class="pageAbout2SecTextBox left">
					<h4 class="h2 bold subColor mb10">色んな人の思いが詰まった図面が建物として形に</h4>
					<div class="pageAboutWorkUl titleBd">
						<ul class="inline_block mb10">
							<li class="bold h4">布池　洋平</li>
							<li class="gray text_m">2013年　中途入社</li>
						</ul>
					</div>
					<p class="gray">大学では地震学を専攻後、不動産営業を経て、データ分析系のコンサルタント業に就く。間接的な業務支援よりも組織の内部に入って自ら事業を育てていくことに興味を持ち、クレストへ。クレストでの現場を主に担当。</p>
					<div class="absolute pageAbout2SecNum fontEnBrush topFvEn bgGradText">02</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="mb50">
			<dl class="pageHospDl mb30">
				<dt class="tra">
					<ul class="inline_block mb10">
						<li class="pageHospHr"><hr></li>
						<li class="bold pageHospTitle h4 subColor">09:00　出社</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inline_block">
						<li class="pageHospImg lh_l">朝礼後、営業ミーティング。1日の営業先を再度確認します。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="pageHospDl mb30">
				<dt class="tra">
					<ul class="inline_block mb10">
						<li class="pageHospHr"><hr></li>
						<li class="bold pageHospTitle h4 subColor">09:00　出社</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inline_block">
						<li class="pageHospImg lh_l">朝礼後、営業ミーティング。1日の営業先を再度確認します。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="pageHospDl mb30">
				<dt class="tra">
					<ul class="inline_block mb10">
						<li class="pageHospHr"><hr></li>
						<li class="bold pageHospTitle h4 subColor">09:00　出社</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inline_block">
						<li class="pageHospImg lh_l">朝礼後、営業ミーティング。1日の営業先を再度確認します。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="pageHospDl mb30">
				<dt class="tra">
					<ul class="inline_block mb10">
						<li class="pageHospHr"><hr></li>
						<li class="bold pageHospTitle h4 subColor">09:00　出社</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inline_block">
						<li class="pageHospImg lh_l">朝礼後、営業ミーティング。1日の営業先を再度確認します。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="pageHospDl mb30">
				<dt class="tra">
					<ul class="inline_block mb10">
						<li class="pageHospHr"><hr></li>
						<li class="bold pageHospTitle h4 subColor">09:00　出社</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inline_block">
						<li class="pageHospImg lh_l">朝礼後、営業ミーティング。1日の営業先を再度確認します。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="pageHospDl mb30">
				<dt class="tra">
					<ul class="inline_block mb10">
						<li class="pageHospHr"><hr></li>
						<li class="bold pageHospTitle h4 subColor">09:00　出社</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inline_block">
						<li class="pageHospImg lh_l">朝礼後、営業ミーティング。1日の営業先を再度確認します。</li>
					</ul>
				
				</dd>
			</dl>
		</div>
		<div class="row mb50" data-aos="fade-up">
			<div class="col-sm-6">
				<img class="mb30" src="<?php echo get_template_directory_uri();?>/img/page_staff_02.jpg" alt="">
			</div>
			<div class="col-sm-6">
				<p class="grayColor bold">Q1.この仕事の魅力って一言で言うと何でしょうか？</p>
				<h4 class="mainColor  bold h3 mb30">建物に機能を与え、命を吹き込むこと</h4>
				<p class="grayColor">この仕事の魅力は、色んな人の思いが詰まった図面が次第に建物として形になっていくことでです。それを日々、間近で感じながら業務に取り組んでいます。やはり図面通りのものが予定内の工期で終わると達成感が大きく、日々成長できる環境に満足しています。クレストでは入社後の先輩方や同僚のフォローも手厚く働きやすい環境が整っていると思います。</p>
			</div>
		</div>
	</div>
	
</section>


<section class="parallaxTitleSec relative topFv">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/top_qa_01.jpg">
		<div class="relative parallaxTitleText">
			<p class="fontEn h1 white text-center">Culture</p>
			<h3 class="h3 bold white mb50 text-center">クレストの社風</h3>
		</div>
	</div>
	
</section>


<section class="pageAboutStrength margin">
	<div class="container">
		<div class="row" data-aos="fade-up">
			<div class="col-sm-4">
				<div class="pageAboutStrBox mb30">
					<img class="pageAboutStrIco" src="<?php echo get_template_directory_uri();?>/img/page_about_ico_01.png" alt="">
					<p class="fontEn h3 mainColor text-center">Trust</p>
					<h4 class="h3 bold subColor text-center titleBd">信頼第一</h4>
					<p class="text_m gray">障害を最小限に、
顧客満足度を最大限に。
全てのお客さまの信頼を守ります。</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="pageAboutStrBox mb30">
					<img class="pageAboutStrIco" src="<?php echo get_template_directory_uri();?>/img/page_about_ico_02.png" alt="">
					<p class="fontEn h3 mainColor text-center">Trust</p>
					<h4 class="h3 bold subColor text-center titleBd">信頼第一</h4>
					<p class="text_m gray">障害を最小限に、
顧客満足度を最大限に。
全てのお客さまの信頼を守ります。</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="pageAboutStrBox mb30">
					<img class="pageAboutStrIco" src="<?php echo get_template_directory_uri();?>/img/page_about_ico_03.png" alt="">
					<p class="fontEn h3 mainColor text-center">Trust</p>
					<h4 class="h3 bold subColor text-center titleBd">信頼第一</h4>
					<p class="text_m gray">障害を最小限に、
顧客満足度を最大限に。
全てのお客さまの信頼を守ります。</p>
				</div>
			</div>
		</div>
	</div>
</section>



<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>