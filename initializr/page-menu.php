<?php get_header(); ?>

<main>

<section class="pageFv mb50">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-7" data-aos="fade-right">
				<img class="" src="<?php echo get_template_directory_uri();?>/img/page_menu_fv.jpg" alt="ビアアイランドのメニュー">
			</div>
			<div class="col-sm-5" data-aos="fade-up">
				<div class="textColorBox whiteBoxRightWide relative bgSubColor">
					<p class="fontEn h00 mainColor mb0 fontEnNegaMa">Our Menu</p>
					<h3 class="bold mainColor h4 mb30">ビアアイランドのメニュー</h3>
					<p class="mb30">生樽クラフトビールはもちろん、ビアカクテル、ソフトドリンク、
ビールに合うフードにコースメニューまで。</p>
				</div>
			</div>
		</div>
	</div>
</section>



<section class="topMenu margin">
	<div class="container">
		
		<?php get_template_part('parts/temp-menu'); ?>
		
	</div>
</section>



<section class="topMenu margin" id="beer">
	<div class="container">
		<div class="text-center">
			<div class="titleBd">
				<p class="fontEn h00 white mb0 fontEnNegaMa">Craft Beer</p>
				<h3 class="bold white h4 mb30">クラフトビール</h3>
			</div>
		</div>
		<img class="mb30" src="<?php echo get_template_directory_uri();?>/img/page_menu_beer_main.jpg" alt="クラフトビー">
		<p class="width780 white mb50">ビアソムリエの資格を持つオーナーが、愛情込めて注ぐ樽生クラフトビールをぜひ、ご堪能ください。旬なクラフトビールが週替わりで<span class="bold">常時10種類以上！</span><span class="bold">"樽生"</span>だからこそクラフトビールの飲み比べも出来ます。</p>
		<div class="row">
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">クラフトビール5種類飲み比べセット</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">1,800Yen</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">ねこにひき / 伊勢角屋麦酒(三重) Sサイズ</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">800yen</p>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">BIG BANG IPA /宇宙ブルーイング(山梨) Sサイズ</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">800Yen</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">桃ヴァイツェン / 箕面ビール(大阪) Sサイズ</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">750Yen</p>
					</li>
				</ul>
			</div>
		</div>
		<div class="row mb50">
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">PORTER / Voyager Brewing(和歌山) Sサイズ</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">750Yen</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">インドの青鬼 / ヤッホーブルーイング(長野) Sサイズ</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">650Yen</p>
					</li>
				</ul>
			</div>
		</div>
		<!--
		<div class="row">
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">クラフトビール5種類飲み比べセット</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">1,800Yen</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">COPPER / Voyager Brewing(和歌山) Sサイズ</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">780yen</p>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">インドの青鬼 / ヤッホーブルーイング(長野) Sサイズ</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">680Yen</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">ヒメホワイト / 伊勢角屋麦酒(三重) Sサイズ</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">680Yen</p>
					</li>
				</ul>
			</div>
		</div>
		<div class="row mb50">
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">BIG BANG IPA / 宇宙ブルーイング(山梨) Sサイズ</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">780Yen</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">バイツェンメルク / 作州津山ビール(岡山) Sサイズ</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">680Yen</p>
					</li>
				</ul>
			</div>
		</div>
		-->
        <p class="white text-center mb30">※上記は一例で、季節によって変化します。</p>
		<a href="https://r.gnavi.co.jp/a9zdmcxm0000/menu3/" target="_blank" class="button fontEn bold white tra text-center mb30" style="max-width:650px;">クラフトビールの詳しいメニューはこちら</a>
	</div>
</section>


<section class="topMenu padding bgMainColor" id="cocktail">
	<div class="container">
		<div class="text-center">
			<div class="titleBd">
				<p class="fontEn h00 mb0 fontEnNegaMa mainColor">Beer Cocktail</p>
				<h3 class="bold mainColor h4 mb30">ビアカクテル・その他ドリンク</h3>
			</div>
		</div>
		<img class="mb30" src="<?php echo get_template_directory_uri();?>/img/page_menu_bc_main.jpg" alt="ビアカクテル・その他ドリンク">
		<p class="width780 mb50">ビアカクテルでビール好きへの第一歩を踏み出しませんか？
麦酒島はビールが好きになれるビアカクテルもとても充実しています。
ノンアルコールカクテル、ソフトドリンクもあります。</p>
		<div class="row">
			<div class="col-sm-4">
				<div class="mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_menu_bc_01.jpg" alt="瀬戸キュンレモネードビア">
					<h4 class="h5 bold mainColor titleBdBottom">瀬戸キュンレモネードビア</h4>
					<p class="fontEnSerif mainColor">700Yen</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_menu_bc_02.jpg" alt="空色アイランドビア">
					<h4 class="h5 bold mainColor titleBdBottom">空色アイランドビア</h4>
					<p class="fontEnSerif mainColor">700Yen</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_menu_bc_03.jpg" alt="ファジレッドビア">
					<h4 class="h5 bold mainColor titleBdBottom">ファジレッドビア</h4>
					<p class="fontEnSerif mainColor">700Yen</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold mainColor">大人のシャンディガフ</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif mainColor mb0">680Yen</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold mainColor">ピーチビア</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif mainColor mb0">680Yen</p>
					</li>
				</ul>
			</div>
		</div>
		<div class="row mb50">
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold mainColor">カシスビア</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif mainColor mb0">680Yen</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold mainColor">桃太郎レッドアイ</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif mainColor mb0">700Yen</p>
					</li>
				</ul>
			</div>
		</div>
		<a href="https://r.gnavi.co.jp/a9zdmcxm0000/menu2/" target="_blank" class="button fontEn bold white tra text-center mb30" style="max-width:650px;">ビアカクテル・その他の詳しいメニューはこちら</a>
	</div>
</section>


<section class="topMenu margin" id="food">
	<div class="container">
		<div class="text-center">
			<div class="titleBd">
				<p class="fontEn h00 white mb0 fontEnNegaMa">Food</p>
				<h3 class="bold white h4 mb30">料理</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_menu_fd_06.jpg" alt="大人のポテサラ">
					<h4 class="h5 bold white titleBdBottom">大人のポテサラ</h4>
					<p class="fontEnSerif white">500Yen</p>
					<p class="white text_m white">一度食べたら癖になる。おかわり者続出の大人のポテサラ。</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_menu_fd_05.jpg" alt="岡山野菜のシーザーサラダ">
					<h4 class="h5 bold white titleBdBottom">岡山野菜のシーザーサラダ</h4>
					<p class="fontEnSerif white">600Yen</p>
					<p class="white text_m white">お祝いやパーティーにぴったりのブーケサラダに変更できます。</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_menu_fd_04.jpg" alt="2種類のグリルソーセージ">
					<h4 class="h5 bold white titleBdBottom">2種類のグリルソーセージ</h4>
					<p class="fontEnSerif white">750Yen</p>
					<p class="white text_m white">日替わりで2種類のソーセージをご用意。オーナーが厳選を重ねた極上の一品。</p>
				</div>
			</div>
		</div>
		<div class="row mb50">
			<div class="col-sm-4">
				<div class="mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_menu_fd_03.jpg" alt="牛すじと生卵の絶品焼きカレードリア">
					<h4 class="h5 bold white titleBdBottom">牛すじと生卵の絶品焼きカレードリア</h4>
					<p class="fontEnSerif white">750Yen</p>
					<p class="white text_m white">当店の人気NO.1フード！！中からとっろと生卵入り。</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_menu_fd_02.jpg" alt="クラフトビールに漬けたちょっと贅沢な鶏のから揚げ">
					<h4 class="h5 bold white titleBdBottom">クラフトビールに漬けたちょっと贅沢な鶏のから揚げ</h4>
					<p class="fontEnSerif white">850Yen</p>
					<p class="white text_m white">贅沢にもクラフトビールにしっかり漬けた鶏のから揚げです。お肉が柔らかくなりとってもジューシー。やみつき確定！</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_menu_fd_01.jpg" alt="フライドポテト">
					<h4 class="h5 bold white titleBdBottom">フライドポテト</h4>
					<p class="fontEnSerif white">450Yen</p>
					<p class="white text_m white">王道のビールのあて。お得なメガ盛りもあります！</p>
				</div>
			</div>
		</div>
		<!--
		<a href="https://r.gnavi.co.jp/a9zdmcxm0000/menu1/" target="_blank" class="button fontEn bold white tra text-center mb30" style="max-width:650px;">料理のメニューはこちら</a>
		-->
		<a href="https://www.hotpepper.jp/strJ001224316/food/" target="_blank" class="button fontEn bold white tra text-center mb30" style="max-width:650px;">料理のメニューはこちら</a>
	</div>
</section>


<section class="topMenu padding bgMainColor" id="course">
	<div class="container">
		<div class="text-center">
			<div class="titleBd">
				<p class="fontEn h00 mb0 fontEnNegaMa mainColor">Course Meal</p>
				<h3 class="bold mainColor h4 mb30">コース</h3>
			</div>
		</div>
		<img class="mb30" src="<?php echo get_template_directory_uri();?>/img/page_menu_course_main.jpg" alt="コース">
		<p class="width780 mb50">パーティ、合コン、女子会、歓迎会、宴会、忘年会、貸切に。3,500円、4,500円、5,500円の3つの大皿パーティーコースがございます　※要予約</p>
		<a href="https://www.hotpepper.jp/strJ001224316/" target="_blank" class="button fontEn bold white tra text-center mb30" style="max-width:650px;">コースの詳しいメニューはこちら</a>
	</div>
</section>



<section class="topAbout spBgBlock pageMenuService">
	<div class="WhiteBox">
		<div class="container">
			<div class="text-center">
				<div class="titleBd mainColor">
					<p class="fontEn h00 mb0 fontEnNegaMa">Use Cases</p>
					<h3 class="bold h4 mb30">様々な利用シーン</h3>
				</div>
			</div>
			<p class="mb30 width780">麦酒島の利用の仕方は様々。一人、友人と飲みにくるも良し、仲間と一緒に盛り上がりたいなら店舗丸ごと貸切るも良し、行事やイベントがあるなら、出張ビアバーテンダーサービスも可能です。</p>
			<div class="row padding">
				<div class="col-sm-7" data-aos="fade-right">
					<div class="WhiteBoxLeft relative">
						<img class="" src="<?php echo get_template_directory_uri();?>/img/pagepage_menu_01.jpg" alt="貸し切りのご利用">
					</div>
				</div>
				<div class="col-sm-5" data-aos="fade-left">
					<div class="textColorBox whiteBoxRight relative bgSubColor">
						<p class="fontEn h00 mainColor mb0 fontEnNegaMa">Reserved</p>
						<h3 class="bold mainColor h4 mb30">貸し切りのご利用</h3>
						<p class="mb30">小さいお店だからこそ出来るサービス。お店全体が完全個室です！
3種類の2h飲み放題付きコースから選んでいただき、10名様〜最大約17名様まで着席でコース利用いただけます。誕生日会やママ友会、貸切ライブなどにオススメです。</p>
					</div>
				</div>
			</div>
			<div class="row padding pageMenuServiceCont">
				<div class="col-sm-6 col-sm-push-6" data-aos="fade-left">
					<div class="WhiteBoxLeft relative">
						<img class="" src="<?php echo get_template_directory_uri();?>/img/page_menu_catering.png" alt="出張ビアバーテンダーサービス">
					</div>
				</div>
				<div class="col-sm-6 col-sm-pull-6" data-aos="fade-right">
					<div class="textColorBox whiteBoxRight relative bgSubColor">
						<p class="fontEn h00 mainColor mb0 fontEnNegaMa">Catering</p>
						<h3 class="bold mainColor h4 mb30">出張ビアバーテンダーサービス</h3>
						<p class="mb30">バーベキューや懇親会、同窓会、会社のイベントなどビアソムリエ資格をもつオーナー自身が出張バーテンダーとして現地へうかがいます。新鮮なクラフトビールをお届けします！</p>
					</div>
				</div>
			</div>
			<!--
			<div class="row padding">
				<div class="col-sm-7" data-aos="fade-right">
					<div class="WhiteBoxLeft relative">
						<img class="" src="<?php echo get_template_directory_uri();?>/img/page_menu_03.jpg" alt="">
					</div>
				</div>
				<div class="col-sm-5" data-aos="fade-left">
					<div class="textColorBox whiteBoxRight relative bgSubColor">
						<p class="fontEn h00 mainColor mb0 fontEnNegaMa">Shopping</p>
						<h3 class="bold mainColor h4 mb30">お酒の通販</h3>
						<p class="mb30">ＢＡＲの醍醐味であるカウンター。
マスターとの会話、隣の方との出会い、一人の時間を過ごす。
そんな時間が過ごせる場所。
地下一階には秘密の場所をご用意いたしました。
使い方はご想像次第。
知れば知るほど面白くなります。</p>
					</div>
				</div>
			</div>
			-->
		</div>
	</div>
</section>




<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>