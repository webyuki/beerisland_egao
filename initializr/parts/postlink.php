					<div class="postLink clearfix">
						<?php 
							//プラグイン 同ターム内で回せる
							previous_post_link_plus(
								array(
									'in_same_tax' => true,
									'format' => '<span class="pre_post_link">&laquo;%link</span>'
								)
							);
							next_post_link_plus(
								array(
									'in_same_tax' => true,
									'format' => '<span class="next_post_link">%link&raquo;</span>'
								)
							);
						?>
					</div>
