<?php get_header(); ?>

<main>

<section class="pageFv">
	<div class="container">
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-7" data-aos="fade-right">
				<img class="" src="<?php echo get_template_directory_uri();?>/img/page_about_fv.jpg" alt="麦酒島 - ビアアイランド - について">
			</div>
			<div class="col-sm-5" data-aos="fade-left">
				<div class="textColorBox whiteBoxRightWide relative bgSubColor">
					<p class="fontEn h00 mainColor mb0 fontEnNegaMa">About Us</p>
					<h3 class="bold mainColor h4 mb30">麦酒島 - ビアアイランド - について</h3>
					<p class="mb30">人目につきにくい隠れ家的ダイニングバー「麦酒島」。ビアソムリエの資格をもつオーナーが厳選した樽生クラフトビールを堪能できます。</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="topMenu padding">
	<div class="container">
		<div class="text-center">
			<div class="titleBd">
				<p class="fontEn h00 white mb0 fontEnNegaMa">Thought</p>
				<h3 class="bold white h4">クラフトビールへの想い</h3>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row padding">
			<div class="col-sm-6 col-sm-push-6" data-aos="fade-left">
				<img class="mb30" src="<?php echo get_template_directory_uri();?>/img/page_about_01.jpg" alt="本物の樽生クラフトビールを">
			</div>
			<div class="col-sm-6 col-sm-pull-6" data-aos="fade-right">
				<div class="fluidTextBox white">
					<h4 class="bold h3 mb30">本物の樽生クラフトビールを</h4>
					<p class="mb30">クラフトビールに初めて出会った時の感動と驚きをたくさんの方に伝えたいと思い、地元岡山で開業する事を決意しました。</p>
					<p class="mb30">日本酒、焼酎、ワイン、ウイスキーの様にビールにも様々な味わいがあります。
現在、日本でクラフトビールを醸造しているブルワリーさんは300社を超えております。
全国各地でそれぞれの個性を活かしたビールを、ビアソムリエの資格を取得しているマスターが厳選セレクトしてラインナップしてます。
</p>
					<p class="mb30">また、ビールは鮮度と管理が命です。
どんなに美味しいビールを作っていただいても管理をしっかりしていないと100%美味しさを表現する事が出来ません。
100%の美味しさを表現出来るように日々管理に精進しておりますので、是非《本物の樽生ビール》を体験してください。</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6" data-aos="fade-right">
				<img class="" src="<?php echo get_template_directory_uri();?>/img/page_about_02.jpg" alt="小萩 倫久">
			</div>
			<div class="col-sm-6" data-aos="fade-left">
				<div class="fluidTextBoxRight white">
					<div class="mb30"></div>
					<p class="text_m fontEnSerif mb0">Owner</p>
					<p class="bold h4 titleBdBottom mb30">小萩 倫久 <span class="text_m gray fontEnSerif">Kohagi Michihisa</span></p>
					<p class="text_m gray">大阪でミュージシャンとして約10年間活動。バンド解散をきっかけにビアソムリエという資格と出会いクラフトビールの魅力に取り憑かれたビール好き。クラフトビールを通じて多くの人を「笑顔」にする為に店舗経営、イベントの主催、各種イベントへの出張ビアバーテンダーとして活動中。</p>
				</div>
			</div>
		</div>
	</div>
</section>





<section class="topAbout pageMenuService spBgBlock">
	<div class="WhiteBox">
		<div class="container">
			<div class="text-center">
				<div class="titleBd mainColor">
					<p class="fontEn h00 mb0 fontEnNegaMa">Features</p>
					<h3 class="bold h4 mb30">麦酒島の魅力</h3>
				</div>
			</div>
			<p class="width780">麦酒島では、オーナーとの会話、隣の方との出会い、一人でも仲間とも楽しめる、そんな時間を過ごすことができます。隠れ家の使い方はあなた次第。知れば知るほど楽しくなります。</p>
			<div class="row padding">
				<div class="col-sm-6" data-aos="fade-right">
					<div class="WhiteBoxLeft relative">
						<img class="" src="<?php echo get_template_directory_uri();?>/img/page_menu_01.jpg" alt="隠れ家的ダイニングバーで楽しい時間を">
					</div>
				</div>
				<div class="col-sm-6" data-aos="fade-left">
					<div class="textColorBox whiteBoxRight relative bgSubColor">
						<p class="fontEn h00 mainColor mb0 fontEnNegaMa">Dining Bar</p>
						<h3 class="bold mainColor h4 mb30">隠れ家的ダイニングバーで楽しい時間を</h3>
						<p class="mb30">岡山市北区田町にある、人目につきにくい隠れ家的ダイニングバー「麦酒島」。そこで時間を気にせず、ゆったりと思いっきりクラフトビールを楽しむことができます。</p>
					</div>
				</div>
			</div>
			<div class="row padding pageMenuServiceCont">
				<div class="col-sm-6 col-sm-push-6" data-aos="fade-left">
					<div class="WhiteBoxLeft relative">
						<img class="" src="<?php echo get_template_directory_uri();?>/img/page_menu_02.jpg" alt="ビアソムリエが厳選したクラフトビールやフードを堪能">
					</div>
				</div>
				<div class="col-sm-6 col-sm-pull-6" data-aos="fade-right">
					<div class="textColorBox whiteBoxRight relative bgSubColor">
						<p class="fontEn h00 mainColor mb0 fontEnNegaMa">Beer Sommelier</p>
						<h3 class="bold mainColor h4 mb30">ビアソムリエが厳選したクラフトビールやフードを堪能</h3>
						<p class="mb30">季節限定の樽生クラフトビールを週替わりでラインナップ。いつ上陸しても新しいビールとの出会いがあります。また、ビアソムリエの資格を活かしてビールとのペアリングを考えたフードラインナップとなっており、より一層ビールが美味しくいただけます。</p>
					</div>
				</div>
			</div>
			<div class="row padding">
				<div class="col-sm-7" data-aos="fade-right">
					<div class="WhiteBoxLeft relative">
						<img class="" src="<?php echo get_template_directory_uri();?>/img/page_menu_03.jpg" alt="店舗貸切やパーティーコースも">
					</div>
				</div>
				<div class="col-sm-5" data-aos="fade-left">
					<div class="textColorBox whiteBoxRight relative bgSubColor">
						<p class="fontEn h00 mainColor mb0 fontEnNegaMa">Party Course</p>
						<h3 class="bold mainColor h4 mb30">店舗貸切やパーティーコースも</h3>
						<p class="mb30">小さなお店だからこそ出来る10名様からの少人数貸切で、あなたの希望どおりのパーティーにも対応可能です。岡山では珍しいクラフトビールの飲み放題付きパーティコースもご用意。一次会としてもご利用可能です。</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="">
	<div class="WhiteBox">
		<div class="container">
			<div class="text-center">
				<div class="titleBd mainColor">
					<p class="fontEn h00 mb0 fontEnNegaMa">Event & Voice</p>
					<h3 class="bold h4 mb30">過去に行ったイベント・お客さまの声</h3>
				</div>
			</div>
			<div class="row mb50" data-aos="fade-up">
			
				<?php
					//$paged = (get_query_var('page')) ? get_query_var('page') : 1;
					$paged = get_query_var('page');
					$args = array(
						'post_type' =>  'post', // 投稿タイプを指定
						'paged' => $paged,
						'posts_per_page' => 4, // 表示するページ数
						'orderby'=>'date',
						'cat' => 4,
						'order'=>'DESC'
								);
					$wp_query = new WP_Query( $args ); // クエリの指定 	
					while ( $wp_query->have_posts() ) : $wp_query->the_post();
						//ここに表示するタイトルやコンテンツなどを指定 
					get_template_part('content-post-top'); 
					endwhile;
					//wp_reset_postdata(); //忘れずにリセットする必要がある
					wp_reset_query();
				?>		
			
			</div>
			<a href="<?php echo home_url();?>/category/event/" class="button fontEn bold ml0 white tra text-center h4">MORE</a>
			
		</div>
	</div>
</section>



<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>