<?php get_header(); ?>

<main>

<section class="pageHeader relative">
	<div class="bgGrad pageHeaderText relative" data-aos="fade-right">
		<p class="pageHeaderEn fontEnBrush white">Recruit</p>
		<h3 class="h2 bold white">採用情報</h3>
	</div>
	<div class="pageHeaderImgBox bgImg absolute" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_qa_01.jpg')" data-aos="fade-left"></div>
	
</section>

<section class="pageAboutStrength margin">
	<div class="container">
		<p class="fontEn h1 mainColor text-center">Step</p>
		<h3 class="h3 bold subColor mb50 text-center">採用応募の流れ</h3>
		<div class="row" data-aos="fade-up">
			<div class="col-sm-4">
				<div class="pageAboutStrBox mb30">
					<img class="pageAboutStrIco" src="<?php echo get_template_directory_uri();?>/img/page_about_ico_01.png" alt="">
					<p class="fontEn h3 mainColor text-center">01</p>
					<h4 class="h3 bold subColor text-center titleBd">履歴書の提出</h4>
					<p class="text_m gray">障害を最小限に、
顧客満足度を最大限に。
全てのお客さまの信頼を守ります。</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="pageAboutStrBox mb30">
					<img class="pageAboutStrIco" src="<?php echo get_template_directory_uri();?>/img/page_about_ico_02.png" alt="">
					<p class="fontEn h3 mainColor text-center">02</p>
					<h4 class="h3 bold subColor text-center titleBd">第一面接</h4>
					<p class="text_m gray">障害を最小限に、
顧客満足度を最大限に。
全てのお客さまの信頼を守ります。</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="pageAboutStrBox mb30">
					<img class="pageAboutStrIco" src="<?php echo get_template_directory_uri();?>/img/page_about_ico_03.png" alt="">
					<p class="fontEn h3 mainColor text-center">03</p>
					<h4 class="h3 bold subColor text-center titleBd">第二面接</h4>
					<p class="text_m gray">障害を最小限に、
顧客満足度を最大限に。
全てのお客さまの信頼を守ります。</p>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="margin">
	<div class="container">
		<p class="fontEn h1 mainColor text-center">Detail</p>
		<h3 class="h3 bold subColor mb50 text-center">募集要項</h3>
		<div class="pageAboutCompanyUl width780 mb50" data-aos="fade-up">
			<ul>
				<li>勤務時間</li>
				<li>平日・土曜日
9:00 ~ 12:00 / 15:00 ~ 20:00</li>
			</ul>
			<ul>
				<li>休日・休暇</li>
				<li>週休 1.5 日または週休 2 日制(どちらかお選びいただけます。) 
年末年始、GW、夏季休暇</li>
			</ul>
			<ul>
				<li>募集資格</li>
				<li>特になし
</li>
			</ul>
			<ul>
				<li>雇用形態</li>
				<li>正社員(常勤)</li>
			</ul>
			<ul>
				<li>待遇</li>
				<li>27万~40万(試用期間3ヶ月は21万~) 
※学生アルバイトはご相談ください 
※待遇は能力によりどんどん上がります</li>
			</ul>
			<ul>
				<li>賞与</li>
				<li>年2回(各1ヶ月分支給) ※能力・業績による</li>
			</ul>
			<ul>
				<li>福利厚生</li>
				<li>社会保険・厚生年金・労災保険・雇用保険、 
セミナー・研修費用補助制度あり</li>
			</ul>
			<ul>
				<li>通勤</li>
				<li>駐車場完備、マイカー・バイク通勤OKです(車通勤は要相談)</li>
			</ul>
		</div>
		<a href="<?php echo home_url();?>/contact" class="button bgGrad bold white tra text-center">クレストの求人に応募する</a>
	</div>
</section>



<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>