<?php get_header(); ?>
<main>

<section class="relative topFvSection">
	<div class="topFv">
		<div class="main_imgBox">
			<div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_01.jpg');"></div>
			<div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_02.jpg');"></div>
			<div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_03.jpg');"></div>
			<div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_04.jpg');"></div>
		</div>
	</div>
	<div class="topFvBoxText white absolute text-center">
		<img class="topFvLogo" src="<?php echo get_template_directory_uri();?>/img/logo.png" alt="岡山でクラフトビールが飲めるバー">
		<p class="fontEn h00 mb0 fontEnNegaMa">Welcome To BeerIsland</p>
		<h3 class="bold h4 mb30">岡山でクラフトビールが飲めるビアバー</h3>
	</div>
</section>

<section class="topAbout">
	<div class="WhiteBox">
		<div class="container">
			<div class="row">
				<div class="col-sm-6" data-aos="fade-right">
					<div class="WhiteBoxLeft relative">
						<img class="" src="<?php echo get_template_directory_uri();?>/img/top_about_01.jpg" alt="本物の樽生クラフトビールを">
					</div>
				</div>
				<div class="col-sm-6" data-aos="fade-left">
					<div class="textColorBox whiteBoxRight relative bgSubColor">
						<p class="fontEn h00 mainColor mb0 fontEnNegaMa">Craft Beer</p>
						<h3 class="bold mainColor h4 mb30">本物の樽生クラフトビールを</h3>
						<p class="mb30">ビアソムリエの資格をもつオーナーが厳選する、その時期にしか味わえない旬な樽生クラフトビールをぜひ、堪能してください。</p>
						<a href="<?php echo home_url();?>/about-us" class="button fontEn bold ml0 white tra text-center h4">MORE</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>





<section class="topGallery">
	<div class="container">
		<div class="topGalleryBox relative">
			<div class="rellax topGalleryImg tGL01 absolute" data-rellax-speed="-1">
				<img class="" data-aos="fade-up"  data-aos-delay="0" src="<?php echo get_template_directory_uri();?>/img/top_gall_01.jpg" alt="岡山のクラフトビールならビアアイランド。画像その1">
			</div>
			<div class="rellax topGalleryImg tGL02 absolute" data-rellax-speed="0">
				<img class="" data-aos="fade-up" data-aos-delay="100" src="<?php echo get_template_directory_uri();?>/img/top_gall_02.jpg" alt="岡山のクラフトビールならビアアイランド。画像その2">
			</div>
			<div class="rellax topGalleryImg tGL03 absolute" data-rellax-speed="0">
				<img class="" data-aos="fade-up" data-aos-delay="200" src="<?php echo get_template_directory_uri();?>/img/top_gall_03.jpg" alt="岡山のクラフトビールならビアアイランド。画像その3">
			</div>
			<div class="rellax topGalleryImg tGL04 absolute" data-rellax-speed="0">
				<img class="" data-aos="fade-up" data-aos-delay="300" src="<?php echo get_template_directory_uri();?>/img/top_gall_04.jpg" alt="岡山のクラフトビールならビアアイランド。画像その4">
			</div>
			<div class="rellax topGalleryImg tGL05 absolute" data-rellax-speed="1">
				<img class="" data-aos="fade-up" data-aos-delay="400" src="<?php echo get_template_directory_uri();?>/img/top_gall_05.jpg" alt="岡山のクラフトビールならビアアイランド。画像その5">
			</div>
			<div class="rellax topGalleryImg tGL06 absolute" data-rellax-speed="0">
				<img class="" data-aos="fade-up" data-aos-delay="500" src="<?php echo get_template_directory_uri();?>/img/top_gall_06.jpg" alt="岡山のクラフトビールならビアアイランド。画像その6">
			</div>
			<div class="rellax topGalleryImg tGL07 absolute" data-rellax-speed="0">
				<img class="" data-aos="fade-up" data-aos-delay="600" src="<?php echo get_template_directory_uri();?>/img/top_gall_07.jpg" alt="岡山のクラフトビールならビアアイランド。画像その7">
			</div>
			<div class="rellax topGalleryImg tGL08 absolute" data-rellax-speed="-1">
				<img class="" data-aos="fade-up" data-aos-delay="700" src="<?php echo get_template_directory_uri();?>/img/top_gall_08.jpg" alt="岡山のクラフトビールならビアアイランド。画像その8">
			</div>
		</div>
	</div>
</section>

<section class="topTime margin">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-7" data-aos="fade-right">
				<img class="" src="<?php echo get_template_directory_uri();?>/img/top_time_01.jpg" alt="麦酒島の魅力">
			</div>
			<div class="col-sm-5" data-aos="fade-up">
				<div class="textColorBox whiteBoxRightWide relative bgMainColor">
					<p class="fontEn h00 mainColor mb0 fontEnNegaMa">Time of BeerIsland</p>
					<h3 class="bold mainColor h4 mb30">麦酒島の魅力</h3>
					<p class="mb30">麦酒島では、オーナーとの会話、隣の方との出会い、一人でも仲間とも楽しめる、そんな時間を過ごすことができます。隠れ家の使い方はあなた次第。知れば知るほど楽しくなります。</p>
					<a href="<?php echo home_url();?>/about-us" class="button fontEn bold ml0 white tra text-center h4">MORE</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="topMenu margin">
	<div class="container">
		<div class="text-center">
			<div class="titleBd">
				<p class="fontEn h00 white mb0 fontEnNegaMa">Menu</p>
				<h3 class="bold white h4 mb30">メニュー</h3>
			</div>
		</div>
		
		<?php get_template_part('parts/temp-menu'); ?>
		
	<a href="<?php echo home_url();?>/menu" class="button fontEn bold ml0 white tra text-center h4">MORE</a>
	</div>
</section>



<section class="">
	<div class="WhiteBox">
		<div class="container">
			<div class="text-center">
				<div class="titleBd mainColor">
					<p class="fontEn h00 mb0 fontEnNegaMa">News</p>
					<h3 class="bold h4 mb30">お知らせ</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="row mb50" data-aos="fade-up">
					
						<?php
							//$paged = (get_query_var('page')) ? get_query_var('page') : 1;
							$paged = get_query_var('page');
							$args = array(
								'post_type' =>  'post', // 投稿タイプを指定
								'paged' => $paged,
								'posts_per_page' => 6, // 表示するページ数
								'orderby'=>'date',
								/*'cat' => -5,*/
								'order'=>'DESC'
										);
							$wp_query = new WP_Query( $args ); // クエリの指定 	
							while ( $wp_query->have_posts() ) : $wp_query->the_post();
								//ここに表示するタイトルやコンテンツなどを指定 
							get_template_part('content-post-top'); 
							endwhile;
							//wp_reset_postdata(); //忘れずにリセットする必要がある
							wp_reset_query();
						?>		
					
					</div>
				</div>
				<div class="col-sm-6">
					<div class="topFb mb30">
<div class="fb-page" data-href="https://www.facebook.com/beer.island.okayama/" data-tabs="timeline" data-width="500" data-height="500" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true"><blockquote cite="https://www.facebook.com/beer.island.okayama/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/beer.island.okayama/">麦酒島-ビアアイランド-</a></blockquote></div>

					</div>
					<div class="topYoutube">
				        <a href="https://www.youtube.com/channel/UCKFXBoWuGIrvkqSj3zXY_mw" target="_blank">
                            <img class="topYoutubeBanner tra mb50" src="<?php echo get_template_directory_uri();?>/img/top_youtube_banner.jpg" alt="youtubeでクラフトビールのオススメ情報を発信">
				        </a>
					</div>
				</div>
			</div>
			<a href="<?php echo home_url();?>/news" class="button fontEn bold ml0 white tra text-center h4">MORE</a>
			
		</div>
	</div>
</section>







</main>



<?php get_footer(); ?>