<?php get_header(); ?>

<section class="area_single">
	<?php 
		while ( have_posts() ) : the_post();
	?>
	<div class="container">
		<?php get_template_part( 'parts/breadcrumb' ); ?>				
		<div class="row">
			<div class="col-sm-9">
				<div class="entry">
					<div class="title_bg title_margin">
						<h2 class="h2 title_main  bold"><?php the_title();?></h2>
					</div>
					<section class="section">
<?php if (get_field('sub_title')):?>
						<h3 class="h3 title_sub bold title_margin title_sub_bg"><?php echo get_field('sub_title'); ?></h3>
<?php endif;?>
						<p><?php the_content();?></p>
						<div class="row">
<?php
	$before_01 = wp_get_attachment_image_src(get_post_meta($post->ID, 'before_01', true), 'full');
	if (!empty($before_01)):
?>
							<div class="col-xs-6">
								<h4 class="bold">施工前</h4>
								<img class="img-responsive m_bottom" src="<?php 	echo $before_01[0]; ?>">
							</div>
<?php endif;?>
<?php
	$after_01 = wp_get_attachment_image_src(get_post_meta($post->ID, 'after_01', true), 'full');
	if (!empty($after_01)):
?>
							<div class="col-xs-6">
								<h4 class="bold">施工後</h4>
								<img class="img-responsive m_bottom" src="<?php 	echo $after_01[0]; ?>">
							</div>
<?php endif;?>
						</div>
						<div class="row">
<?php
	$before_02 = wp_get_attachment_image_src(get_post_meta($post->ID, 'before_02', true), 'full');
	if (!empty($before_02)):
?>
							<div class="col-xs-6">
								<h4 class="bold">施工前</h4>
								<img class="img-responsive m_bottom" src="<?php 	echo $before_02[0]; ?>">
							</div>
<?php endif;?>
<?php
	$after_02 = wp_get_attachment_image_src(get_post_meta($post->ID, 'after_02', true), 'full');
	if (!empty($after_02)):
?>
							<div class="col-xs-6">
								<h4 class="bold">施工後</h4>
								<img class="img-responsive m_bottom" src="<?php 	echo $after_02[0]; ?>">
							</div>
<?php endif;?>
						</div>
					</section>
					
<?php if(get_field('if_voice')):?>
				<section class="section" id="voice">
						<h3 class="h3 title_sub bold title_margin title_sub_bg">お客様の声</h3>
						<p><?php the_content();?></p>
						<div class="row">
							<div class="col-sm-6">
	<?php
		$voice_img_01 = wp_get_attachment_image_src(get_post_meta($post->ID, 'voice_img_01', true), 'full');
		if (!empty($voice_img_01)):
	?>
							
								<img class="img-responsive m_bottom" src="<?php 	echo $voice_img_01[0]; ?>">
	<?php endif;?>
	<?php
		$voice_img_02 = wp_get_attachment_image_src(get_post_meta($post->ID, 'voice_img_02', true), 'full');
		if (!empty($voice_img_02)):
	?>
								<img class="img-responsive m_bottom" src="<?php 	echo $voice_img_02[0]; ?>">
	<?php endif;?>
							</div>
	<?php if (get_field('voice_text')):?>
							<div class="col-sm-6">
								<p><?php echo get_field('voice_text'); ?></p>
							</div>
	<?php endif;?>
						</div>
					</section>
<?php endif;?>

				</div>
				<?php get_template_part( 'parts/postlink' ); ?>						
			</div>
			<div class="col-sm-3">
				<?php get_sidebar(); ?>
                <?php //get_sidebar(voice); ?>
   			</div>
		</div>
	</div>
	<?php 
		endwhile;
	?>	
</section>
<?php get_footer(); ?>