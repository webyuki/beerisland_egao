<?php get_header(); ?>

<?php 
	while ( have_posts() ) : the_post();
?>

<?php 
	// テキスト・ウィジェット関係
	$demand = get_field("demand");
	$advice = get_field("advice");
	$address = get_field("address");
	$period = get_field("period");
	$age = get_field("age");
	$price = get_field("price");
	$item = get_field("item");
	$exam_1_text = get_field("exam_1_text");
	$exam_2_text = get_field("exam_2_text");
	$voice_text = get_field("voice_text");
	$imgs = get_field("imgs");
	// メイン画像
	if ( has_post_thumbnail() ) {
		$image_id = get_post_thumbnail_id();
		$image_url = wp_get_attachment_image_src ($image_id,'large',true);
	} else {
	}  
?>


<main>

<section class="pageHeader bgMainColor mb50">
	<div class="bgImg bgCircle paddingW imgNone" style="background-image:url('<?php echo get_template_directory_uri();?>/img/bg_circle.png')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="white mb30">
						<p class="fontEn h2">Works</p>
						<h3 class="h3">施工事例</h3>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</section>




<section class="pageWorksDeCont margin">
	<div class="container">
		<div class="width780">
			<div class="text-center">
				<h3 class="h3 mainColor mb30 titleBd"><?php the_title(); ?></h3>
				<ul class="DeContUl inline_block text-center mb30">
					<li>
					
				<?php 
					if ($terms = get_the_terms($post->ID, 'works_cate')) {
						foreach ( $terms as $term ) {
							echo '<div class="pageWorkLiBoxCate white bgMainColor">' . esc_html($term->name) . '</div>';
							$term_slug = $term->slug;
						}
					}
				?>
					</li>
					<li>
						<div class="pageWorkLiBoxDate subColor fontEn text_s"><?php echo get_the_date( 'F d, Y' ); ?></div>
					</li>
				</ul>
			</div>
			
			<div class="DeContBABox relative">
				<img class="DeContAfterImg" src="<?php echo $image_url[0]; ?>" alt="">
				<p class="fontEnSerif h0 italic absolute DeContAfterText">After</p>
				<div class="DeContBeforeBox absolute">
				
					<?php if( get_field('before_img') ):?>
						<img class="DeContBeforeImg" src="<?php the_field('before_img'); ?>" alt="">
						<p class="fontEnSerif h2 italic absolute DeContBeforeText">Before</p>
					<?php endif; ?>				
				
				</div>
			</div>
		</div>
	</div>
</section>

<section class="pageWorksDeCont mb100">
	<div class="container">
		<div class="pageAboutCompanyUl width780 mb50">
			<ul>
				<?php if($demand): ?><li>ご要望</li><li><?php echo $demand; ?></li><?php endif; ?>
			</ul>
			<ul>
				<?php if($advice): ?><li>ご提案内容</li><li><?php echo $advice; ?></li><?php endif; ?>
			</ul>
			<ul>
				<li>施工内容</li><li>

					<?php 
						if ($terms = get_the_terms($post->ID, 'works_cate')) {
							foreach ( $terms as $term ) {
								echo esc_html($term->name) . '　';
								$term_slug = $term->slug;
							}
						}
					?>
				
				</li>
			</ul>
			<ul>
				<?php if($address): ?><li>住所</li><li><?php echo $address; ?></li><?php endif; ?>
			</ul>
			<ul>
				<?php if($period): ?><li>工期</li><li><?php echo $period; ?></li><?php endif; ?>
			</ul>
			<ul>
				<?php if($age): ?><li>築年数</li><li><?php echo $age; ?></li><?php endif; ?>
			</ul>
			<ul>
				<?php if($price): ?><li>費用</li><li><?php echo $price; ?></li><?php endif; ?>
			</ul>
			<ul>
				<?php if($item): ?><li>使用商材</li><li><?php echo $item; ?></li><?php endif; ?>
			</ul>
		</div>
		<div class="width780">
			<h4 class="h3 mainColor mb30 titleBd">スタッフからのコメント</h4>
			<div class="entry">
				<?php the_content();?>
			</div>
			
		</div>
		<div class="width780 mb50">
			<h4 class="h3 mainColor mb30 titleBd">お客様の声</h4>
			<div class="pageWorksDeContVoiceTextArea">
				<?php if($voice_text): ?><?php echo $voice_text; ?><?php endif; ?>
			</div>
		</div>
		<?php if($imgs): ?><div class="width780 mb50"><?php echo $imgs; ?></div><?php endif; ?>
		
			
		
	</div>

</section>



<section class="pageWorkLi padding bgSubColor">
	<div class="container">
		<p class="fontEn h2 mainColor text-center">Others</p>
		<h3 class="h3 mainColor mb30 text-center">その他の事例を見る</h3>
		<ul class="pageWorkLiUl inline_block">

        	<?php
						$args = array(
						'post_type' => 'works', //投稿タイプ名
						'posts_per_page' => 4, //出力する記事の数
						'tax_query' => array(
						array(
						'taxonomy' => 'works_cate', //タクソノミー名
						'field' => 'slug',
						'terms' => $term_slug //タームのスラッグ
						)
						)
						);
					?>
                    <?php
                    $myposts = get_posts( $args );
                	foreach ( $myposts as $post ) : setup_postdata( $post );
					?>

			<?php get_template_part('content-post-works-archive'); ?>

            <?php endforeach; ?>

		</ul>
		<a href="<?php echo home_url();?>/works" class="button white tra text-center">他の実績を詳しく見る</a>
	</div>
	
</section>



</main>

<?php 
	endwhile;
?>	


<?php get_footer(); ?>


