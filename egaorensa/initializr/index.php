<?php get_header(); ?>
<main>

<section class="pageHeader" id="">
    <div class="container" data-aos="fade-up">
        <div class="text-center mb50">
            <p class="fontEn h1 titleBd titleBdBlack inlineBlock mb10">News</p>
            <h3 class="serif h3">新着情報</h3>
        </div>
    </div>
</section>




        

<section class="pageNews margin">
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<?php
					while ( have_posts() ) : the_post();
						get_template_part('content-post'); 
					endwhile;
				?>
			</div>
			<div class="col-sm-3">
				<?php dynamic_sidebar(); ?>
			</div>
		</div>
		<?php get_template_part( 'parts/pagenation' ); ?>
	</div>
</section>
</main>
<?php get_footer(); ?>