<?php get_header(); ?>

<main>

<section class="relative" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/page_recruit_fv_01.jpg">
        <div class="bgWhiteTrans paddingW">
            <div class="container" data-aos="fade-up">
                <div class="text-center">
                    <p class="fontEn h3 mb0 mainColor">RECRUIT</p>
                    <h3 class="h2 bold">採用情報</h3>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="margin">
    <div class="container">
        <div class="text-center mb50">
            <p class="fontEn h4 mb0 mainColor">MESSAGE</p>
            <h3 class="h3 bold">代表あいさつ</h3>
        </div>
        <div class="text-center width720">
            <h4 class="h3 bold mainColorLight mb30">必要なのは「お客さまの役に立ちたい」気持ち</h4>
      
<div class="mb30">
    <p>私たちの仕事は、「お客さまの快適な暮らしを守ること」。</p>

    <p>言葉にするのは簡単な一方で、技術と知識を必要とする奥の深い仕事です。</p>

    <p>人が一生を過ごす住まいには様々な機能を持つ設備があり、非常に複雑なつくりであるにもかかわらず、「普通に使えることが当たり前」。</p>

    <p>能力を磨く道の途中には、投げ出したくなることがあるかもしれません。</p>

    <p>けれどお客さまの悩みに寄り添い、仲間の言葉に耳をかたむける気持ちがあれば、近い将来、必ず「次もお願いね」と言ってもらえるようになる。それがオカザキリフォームラボの仕事です。</p>
</div>


<h5 class="bold h4 mb10">仕事を通して地域と社会に貢献し、自分の幸せを実現する</h5>

<div class="mb30">
    <p>ここで身につけた知識や技術は、「大好きなわが家にずっと住み続けたい」と願うお客さまを通して地域の人たちの幸せに貢献し、働くあなたの人生を充実したものにしてくれるはずです。</p>

    <p>応募資格は「お客さまの役に立ちたい」気持ち。</p>

    <p>未来を一緒に作っていける人からのご連絡を、心から楽しみにしています。</p>
</div>



            <img class="mb30" src="<?php echo get_template_directory_uri();?>/img/page_recruit_message_01.jpg" alt="">
            <p class="mb0">代表取締役</p>
            <p class="h3 bold">岡崎 晋典</p>
        </div>
    </div>
</section>

<section class="padding bgMainColorLight">
    <div class="container">
        <div class="text-center mb50">
            <p class="fontEn h4 mb0 mainColor">WANTED</p>
            <h3 class="h3 bold">求める人物像</h3>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="pageRecruitWantedBox mb30">
                    <img src="<?php echo get_template_directory_uri();?>/img/page_recruit_wanted_01.png" alt="" class="pageRecruitWantedIco mb10">
                    <h4 class="bold mainColorLight h4 text-center mb30">素直な人</h4>
                    <p>リフォームのご相談や工事では、柔軟な対応が必要です。お客さまや先輩の言葉に素直に耳をかたむけ、違った考え方も受け入れられる人を求めています。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageRecruitWantedBox mb30">
                    <img src="<?php echo get_template_directory_uri();?>/img/page_recruit_wanted_02.png" alt="" class="pageRecruitWantedIco mb10">
                    <h4 class="bold mainColorLight h4 text-center mb30">あいさつができる人</h4>
                    <p>オカザキリフォームラボでは、お客さまや関わる人へのあいさつを大切にしています。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageRecruitWantedBox mb30">
                    <img src="<?php echo get_template_directory_uri();?>/img/page_recruit_wanted_03.png" alt="" class="pageRecruitWantedIco mb10">
                    <h4 class="bold mainColorLight h4 text-center mb30">前向きな人</h4>
                    <p>リフォームに関する技術や商品は日々進歩しています。お客さまにとって最善のサービスを提供するため、前向きに努力できる人を求めています。</p>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="margin">
    <div class="container">
        <div class="text-center mb50">
            <p class="fontEn h4 mb0 mainColor">INTERVIEW</p>
            <h3 class="h3 bold">スタッフインタビュー</h3>
        </div>
        <div class="row mb30">
            <div class="col-sm-6 col-sm-push-6">
                <div class="">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_recruit_inter_01.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-6 col-sm-pull-6">
                <h4 class="topServiceTitle relative bold h3 mb30"><span class="bold">お客様の笑顔のために</span></h4>
                <p class="text_m mb10 gray">リフォーム担当</p>
                <p class="h4 bold mb30">田中 一郎</p>
                <div class="mb30">
                    <p>この仕事のやりがいは、なんといっても身につけた技術を活かせること。この世界に入ったばかりの頃は、仕事の下手間や先輩社員の手伝いをしながら技術を学ぶことでいっぱいいっぱいでした。</p>
                </div>
            </div>
        </div>
        <div class="row mb50">
            <div class="col-sm-6">
                <div class="">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_recruit_inter_01.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-6">
                <p class="grayLight ">Q. どんな時が働いていて一番うれしいですか？</p>
                <h5 class="bold mainColorLight h3 mb30">お客様の笑顔のために</h5>
                <div class="mb30">
                    <p>この仕事のやりがいは、なんといっても身につけた技術を活かせること。この世界に入ったばかりの頃は、仕事の下手間や先輩社員の手伝いをしながら技術を学ぶことでいっぱいいっぱいでした。

この仕事のやりがいは、なんといっても身につけた技術を活かせること。この世界に入ったばかりの頃は、仕事の下手間や先輩社員の手伝いをしながら技術を学ぶことでいっぱいいっぱいでした。</p>
                </div>
            </div>
        </div>
        <div class="row mb30">
            <div class="col-sm-6 col-sm-push-6">
                <div class="">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_recruit_inter_01.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-6 col-sm-pull-6">
                <h4 class="topServiceTitle relative bold h3 mb30"><span class="bold">お客様の笑顔のために</span></h4>
                <p class="text_m mb10 gray">リフォーム担当</p>
                <p class="h4 bold mb30">田中 一郎</p>
                <div class="mb30">
                    <p>この仕事のやりがいは、なんといっても身につけた技術を活かせること。この世界に入ったばかりの頃は、仕事の下手間や先輩社員の手伝いをしながら技術を学ぶことでいっぱいいっぱいでした。</p>
                </div>
            </div>
        </div>
        <div class="row mb50">
            <div class="col-sm-6">
                <div class="">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_recruit_inter_01.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-6">
                <p class="grayLight ">Q. どんな時が働いていて一番うれしいですか？</p>
                <h5 class="bold mainColorLight h3 mb30">お客様の笑顔のために</h5>
                <div class="mb30">
                    <p>この仕事のやりがいは、なんといっても身につけた技術を活かせること。この世界に入ったばかりの頃は、仕事の下手間や先輩社員の手伝いをしながら技術を学ぶことでいっぱいいっぱいでした。

この仕事のやりがいは、なんといっても身につけた技術を活かせること。この世界に入ったばかりの頃は、仕事の下手間や先輩社員の手伝いをしながら技術を学ぶことでいっぱいいっぱいでした。</p>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="padding bgSubColor">
	<div class="container">
        <div class="text-center mb50">
            <p class="fontEn h4 mb0 mainColor">DESCRIPTION</p>
            <h3 class="h3 bold">募集要項</h3>
        </div>
        <!--
		<div class="detail1 mb80">
			<div class="width720">
				<h4 class="h4 mb10 titleBd text-center inlineBock mb30">製造スタッフ(未経験者歓迎)</h4>
				<div class="mb30">
				
<p>当社の仕事は「ライン工」と呼ばれる「流れ作業」ではなく依頼された商品の図面を確認し、材料の調達→加工→検査→完成までを一貫して行います。</p>
<p>製造工程の中には<span class="bold">「NC旋盤」や「マシニング加工」</span>といった専門知識を要するものもありますが、まずは、先輩スタッフのもと、各工程の下準備などを行っていただきます。</p>
<p>スタッフ一人ひとりに対し、<span class="bold">各工程ごとで仕事の習得度を一覧にした「スキル管理表」</span>がありますので、少しずつお任せできる仕事を増やしていってください。</p>
<p>年月が経てば、技術者としての成長を実感できるとともに、日本経済の象徴である<span class="bold">「ものづくり」の真髄が学べる職場</span>です。</p>
				
				</div>
			</div>
			<div class="pageAboutCompanyUl width720 mb50" data-aos="fade-up">
				<ul>
					<li>勤務地</li>
					<li>総社市 東阿曽</li>
				</ul>
				<ul>
					<li>給与</li>
					<li>月給 15.6万 〜 30万円</li>
				</ul>
				<ul>
					<li>雇用形態</li>
					<li>正社員</li>
				</ul>
				<ul>
					<li>求める人材</li>
					<li>・高卒以上<br>・未経験者積極採用<br>・プログラミング経験者優遇</li>
				</ul>
				<ul>
					<li>勤務時間</li>
					<li>
						<p>【勤務時間】8:00~17:00　※変形労働時間制<br>【休　日】土曜、日曜</p>
						<p>※繁忙期には一部土曜出勤有　会社カレンダーがあります<br>GW、夏季休暇、年末年始休暇</p>
					</li>
				</ul>
				<ul>
					<li>待遇・福利厚生</li>
					<li>
						<p>・社会保険完備<br>・財形退職金制度　※勤続3年以上<br>・前年賞与実績　年2回<br>・給与は経験などを考慮します</p>
						<p>【手当】<br>・通勤手当　上限10,000円<br>・食事手当　1食130円<br>・家族手当　1人2,000円<br>・皆勤手当　5,000円<br>・役職手当　15,000円~<br>・管理手当　20,000円~<br>※諸条件有</p>
						
					</li>
				</ul>
				<ul>
					<li>その他</li>
					<li>・NC、マシニングなどの「技術」を身に着けることができる職場です。<br>・今後の会社の成長を一緒に支えてくれる若い力を求めています。<br>・職場見学、歓迎します。<br>・不明な点はお気軽にお問合せください</li>
				</ul>
			</div>
		</div>
        -->
        <div class="text-center">
            <a href="<?php echo home_url();?>/contact" class="white button bold tra text-center">求人に応募する</a>
        </div>
	</div>

</section>





<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>