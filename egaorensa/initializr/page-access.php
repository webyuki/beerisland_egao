<?php get_header(); ?>
<main>
<section class="pageHeader bgCenter mb50">
	<div class="pageHeaderCover">
		<div class="container">
			<div class="text-center white pageHeaderTitle">
				<h3 class="titleJp h3 titleBd"><?php the_title();?></h3>
				<p class="titleEn questrial"><?php $post = get_page($page_id); echo $post->post_name;?></p>
			</div>
		</div>
	</div>
</section>


<section class="mb50">
	<div class="container">
		<div class="text-center mb50">
			<h3 class="titleJp h3 titleBd">天満屋ハピータウンリブ21方面からお越しの場合</h3>
		</div>
		
		<div class="row mb50">
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">01</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/access01.jpg" alt="リーブ21さんや総社西中学校さん前の交差点を南へ直進します。">
					<p class="mb10">リブ21さんや総社西中学校さん前の交差点を南へ直進します。</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">02</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/access02.jpg" alt="そのまま中国銀行さんが右手にある交差点もまっすぐ南へ直進します。">
					<p class="mb10">そのまま中国銀行さんが右手にある交差点もまっすぐ南へ直進します。<!--そのまま右手にある中国銀行さんの交差点もまっすぐ南へ直進します。--></p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">03</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/access03.jpg" alt="8番らーめんさんのある交差点を右折すると、すぐ右手に当院が見えてきます。">
					<p class="mb10">8番らーめんさんのある交差点を右折すると、すぐ右手に当院が見えてきます。</p>
				</div>
			</div>
		</div>
		
		<div class="text-center mb50">
			<h3 class="titleJp h3 titleBd">総社大橋方面からお越しの場合</h3>
		</div>
		
		<div class="row mb50">
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">01</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/access11.jpg" alt="総社大橋から市内に向けて、交差点の右から2番目道路へ道なりに進みます">
					<p class="mb10">総社大橋から市内に向けて、交差点の右から2番目道路へ道なりに進みます。</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">02</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/access12.jpg" alt="クラブへアーズさんが左手にある交差点を右折し直進します。">
					<p class="mb10">クラブへアーズさんが左手にある交差点を右折し直進します。</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">03</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/access13.jpg" alt="この跨線橋を超えて、交差点を直進したら、すぐ左手に当院が見えてきます。">
					<p class="mb10">この跨線橋を超えて、交差点を直進したら、すぐ左手に当院が見えてきます。</p>
				</div>
			</div>
		
		</div>
		<div class="text-center mb50">
			<h3 class="titleJp h3 titleBd">総社市東部からお越しの場合</h3>
		</div>
		
		<div class="row mb50">
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">01</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/access20.jpg" alt="100円ショップセリアさんが見える交差点を西へ直進します。">
					<p class="mb10">100円ショップセリアさんが見える交差点を西へ直進します。</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">02</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/access21.jpg" alt="ドリームさんやイリエさんが見える交差点を右折し直進します。">
					<p class="mb10">ドリームさんやイリエさんが見える交差点を右折し直進します。</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">03</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/access22.jpg" alt="左手に8番らーめんさんがある交差点を直進すると右手に当院が見えて">
					<p class="mb10">左手に8番らーめんさんがある交差点を直進すると右手に当院が見えてきます。</p>
				</div>
			</div>
		
		</div>
		<div class="h1 mColor accessGoalEn text-center">＼<span class="questrial bold">GOAL</span>／</div>
		<img class="max400 mb10 img-center" src="<?php echo get_template_directory_uri();?>/img/access_goal.jpg" alt="みなさまのご来院、心よりお待ちしております。">
		<h4 class="h4 bold text-center">みなさまのご来院、心よりお待ちしております。</h4>

		
	</div>
</section>

<section class="topStore">
	<div class="container">
		<h3 class="titleJp h3 titleBd text-center mb50">いわさ整骨院について</h3>
		<div class="row" data-aos="fade-up">
			<div class="col-sm-6">
				<div class="max500">
					<dl class="dl-horizontal access">
						<dt>名称</dt>
						<dd>いわさ整骨院</dd>
						<dt>代表</dt>
						<dd>岩佐　祥生</dd>
						<dt>住所</dt>
						<dd>〒719-1136　総社市駅前二丁目16-107</dd>
						<dt>駐車場</dt>
						<dd>10台完備</dd>
						<dt>電話番号</dt>
						<dd>0120-93-8258</dd>
					</dl>	
					
					<table class="table table-bordered calendar text-center">
						<tbody>
							<tr>
								<th>診療時間</th>
								<th>月</th>
								<th>火</th>
								<th>水</th>
								<th>木</th>
								<th>金</th>
								<th>土</th>
								<th>日</th>
							</tr>
							<tr>
								<td class="time">9:00 - 12:00</td>
								<td>◯</td>
								<td>◯</td>
								<td>◯</td>
								<td><span class="rest">休</span></td>
								<td>◯</td>
								<td>◯</td>
								<td>※</td>
							</tr>
							<tr>
								<td class="time">14:00 - 19:30</td>
								<td>◯</td>
								<td>◯</td>
								<td>◯</td>
								<td><span class="rest">休</span></td>
								<td>◯</td>
								<td>※</td>
								<td><span class="rest">休</span></td>
							</tr>
						</tbody>
					</table>
					<p>※土曜：午後6時半まで<br>※日・祝：午後1時まで</p>
											
				</div>
			</div>
			<div class="col-sm-6">
				<img class="mb10 max500" src="<?php echo get_template_directory_uri();?>/img/recruitgal04.jpg" alt="いわさ整骨院">
			</div>
		</div>
		
	</div>
	<div class="gmap">
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13125.525811979644!2d133.74027!3d34.670321!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcb714c333fb5af98!2z44GE44KP44GV5pW06aqo6Zmi!5e0!3m2!1sja!2sjp!4v1524367681142" width="1920" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</section>




<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>