<?php if(get_field('if_voice')):?>
<li>
	<a href="<?php the_permalink();?>#voice">
<?php
	$voice_img_02 = wp_get_attachment_image_src(get_post_meta($post->ID, 'voice_img_01', true), 'thumb_size');
?>
		<img class="img-responsive" src="<?php echo $voice_img_02[0]; ?>">
		<p class="margin0 h5 bold line_height_m"><?php the_title();?></p>
	</a>
<?php
if($terms = get_the_terms($post->ID, 'cat_example') ):
    foreach ( $terms as $term ):
        $slug = esc_html($term->slug);
        $name = esc_html($term->name);
?>
	<span class=" text_s">#<?php echo $name;?></span>
<?php 
	endforeach;
endif;
?>

</li>

	



<?php endif;?>




