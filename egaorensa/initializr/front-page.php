<?php get_header(); ?>
<main>

<!--
<section class="topFvSection relative">
    <div class="topFv relative">
        <div class="main_imgBox">
            <div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_01.jpg');"></div>
            <div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_shop_bg.jpg');"></div>
            <div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_concept_03.jpg');"></div>
        </div>
    </div>
</section>
-->
<section class="bdBottom">
    <div class="topFv bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_01.jpg');" data-aos="zoom-in"></div>
</section>


<section class="relative" id="concept">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/top_concept_bg.jpg">
        <div class="container">
            <div class="topConceptBox bdBox bgMainColor width980"  data-aos="flip-up">
                <div class="text-center mb30">
                    <p class="fontEn h1 mainColor">CONCEPT</p>
                    <h3 class="white h1 bold titleIco">ビールが繋ぐ、<br class="hidden-lg">“エガオレンサIPA”</h3>
                </div>
                <div class="white">
<p>ガツンとくる衝撃と、華やかに香るホップが特徴の『エガオレンサIPA』。麦酒島初となるオリジナルビールが誕生しました。</p>
<p>名前には、隣同士で座った人やお店で一緒になった人たちに、笑顔が連鎖していくように広がってほしいとの思いを込めています。</p>
<p>エガオレンサIPAが生んだ笑顔が、地域のエネルギーとなるように。楽しい時間をたくさんの人たちと過ごせる場所を、今夜もご用意しています。</p>


                </div>
            </div>
        </div>
    </div>
</section>

<section class="margin" id="feature">
    <div class="titleBd inlineBlock">
        <div class="left titleIco">
            <div class="inlineBlock verticalMiddle">
                <p class="fontEn h1 mainColor">FEATURE</p>
                <h3 class="white h1 bold">エガオレンサIPAの特徴</h3>
            </div>
        </div>
    </div>
    <div class="container width980">
        <div class="row margin">
            <div class="col-sm-4">
                <img class="mb10 topFeatureImg" src="<?php echo get_template_directory_uri();?>/img/top_feature_01.png" alt="ガツンとくるホップの苦" data-aos="flip-left">
            </div>
            <div class="col-sm-8">
                <div class="topFeatureText relative">
                    <h4 class="white bold h3 mb30">ガツンとくる<br>ホップの苦味</h4>
                    <div class="white">
<p>18世紀末、イギリスは植民地支配していたインドに滞在するイギリス人にペールエールを送るため、麦汁濃度を高くし、防腐剤がわりのホップを大量に投入したIPA（Indian Pale Ale）を作りました。</p>
<p>エガオレンサIPAはそのホップによる苦味を特徴とするビールです。</p>
<p>ガツンとした衝撃と後に残る苦味は、クラフトビールファンにはたまらない刺激です。</p>


                    </div>
                    <img class="absolute topFeatureIco ico01 animated" src="<?php echo get_template_directory_uri();?>/img/top_feature_ico_01.png" alt="ガツンとくるホップの苦味">
                </div>
            </div>
        </div>
        <div class="row margin">
            <div class="col-sm-4 col-sm-push-8">
                <img class="mb10 topFeatureImg" src="<?php echo get_template_directory_uri();?>/img/top_feature_01.png" alt="笑顔を広げるフルーティーで華やかな香り" data-aos="flip-right">
            </div>
            <div class="col-sm-8 col-sm-pull-4">
                <div class="topFeatureText left relative">
                    <h4 class="white bold h3 mb30">笑顔を広げる<br>フルーティーで華やかな香り</h4>
                    <div class="white">

<p>グラスを傾けた瞬間に広がるのは、華やかなホップの香り。温度の変化に合わせてゆっくりと形を変えながら、最後の一滴まで豊かな余韻を残します。</p>
<p>コアなクラフトビールファンも、これからおいしさや楽しさを知っていくお客さまも、明るい笑顔を咲かせる香りを存分にご堪能ください。後からくる苦味とのギャップは、思わず誰かとシェアしたくなる感覚です。</p>

                    </div>
                    <img class="absolute topFeatureIco ico02 animated" src="<?php echo get_template_directory_uri();?>/img/top_feature_ico_02.png" alt="笑顔を広げるフルーティーで華やかな香り">
                </div>
            </div>
        </div>
        <div class="row margin">
            <div class="col-sm-4">
                <img class="mb10 topFeatureImg" src="<?php echo get_template_directory_uri();?>/img/top_feature_01.png" alt="ハイアルコールの重厚な口当たり" data-aos="flip-left">
            </div>
            <div class="col-sm-8">
                <div class="topFeatureText relative">
                    <h4 class="white bold h3 mb30">ハイアルコールの<br>重厚な口当たり</h4>
                    <div class="white">
<p>一般的なビールのアルコール度数が５％であるのに対して、じっくり楽しむIPAのアルコール度数は6~6.5％。エガオレンサIPAも同様に、重厚な口当たりが特徴です。</p>
<p>隣り合った人たちとの愉快なコミュニケーションに、喉の奥を刺激する麦芽の風味をプラス。麦酒島での出会いと、共有する時間を深めます。</p>

                    </div>
                    <img class="absolute topFeatureIco ico03 animated" src="<?php echo get_template_directory_uri();?>/img/top_feature_ico_03.png" alt="ハイアルコールの重厚な口当たり">
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(function(){
    setInterval(function(){
        $('.topFeatureIco').toggleClass('bounce');
    },3000);
});

</script>

<section class="relative bdBottom" id="story">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/top_story_bg.jpg">
        <div class="padding">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6 col-sm-push-6">
                        <div class="topStoryTitleBox">
                            <div class="left titleIco mb10">
                                <div class="inlineBlock verticalMiddle">
                                    <p class="fontEn h1 mainColor">STORY</p>
                                    <h3 class="white h1 bold">銘柄への想い</h3>
                                </div>
                            </div>
                            <div class="bgSubColor topStoryBgText mb30">
                                <div>
<p class="bold">『エガオレンサIPA』</p>
<p>この名前には、お店に来てくれたお客さまをビールを通じて笑顔にしたい。その笑顔を隣にいる人やお店全体、地域にも広げたいとの思いを込めました。</p>
<p>クラフトビールのおいしさや楽しさ、味わいの深さがコミュニケーションツールとなり、お客さま同士のつながりが深まることを願いながら、一杯一杯を笑顔でサーブしています。</p>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-sm-pull-6">
                        <ul class="slickJsStory" data-aos="flip-up">
                            <li>
                                <div class="bdBox bgMainColor topStorySlideBox">
                                    <div class="text-center mb30">
                                        <p class="fontEn h3 mainColor">STORY 01</p>
                                        <h4 class="white h3 bold">思い出の『エガオレンサIPA』</h4>
                                    </div>
                                    <img class="mb30" src="<?php echo get_template_directory_uri();?>/img/top_story_img_01.jpg" alt="思い出の『エガオレンサIPA』">
                                    <div class="white">
                                        <p>大阪でのバンド時代、ファンから高い支持を得ていたオリジナル楽曲の『エガオレンサ』。ライブハウスで、路上で、たくさんの笑顔が溢れた時間を思い、初のプロデュースビールにこの名前をつけました。
地元岡山でもまた、麦酒島のエガオレンサIPAで、ここに集まるお客さまや地域の人たちを笑顔にしていきます。
</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="bdBox bgMainColor topStorySlideBox">
                                    <div class="text-center mb30">
                                        <p class="fontEn h3 mainColor">STORY 02</p>
                                        <h4 class="white h3 bold">おいしいから、知らせたい</h4>
                                    </div>
                                    <img class="mb30" src="<?php echo get_template_directory_uri();?>/img/top_story_img_01.jpg" alt="おいしいから、知らせたい">
                                    <div class="white">
                                        <p>麦酒島のラインナップは、コアなクラフトビールファンに楽しんでもらいたい銘柄と、ビールを好きになってもらいたい人のための銘柄、そしてオーナー自身の好みで選んだ銘柄の3種類。「おいしいから、飲んでみてほしい」。エガオレンサIPAもまた、その想いの結晶です。
</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>

$('.slickJsStory').slick({
    dots: true,
    infinite: true,
    /*speed: 500,
    fade: false,*/
    cssEase: 'linear',
    nextArrow: '<div class="slide-arrow next-arrow"></div>',
    prevArrow: '<div class="slide-arrow prev-arrow"></div>'
});
</script>



<section class="relative" id="egaorensa">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/top_egao_bg.jpg">
        <div class="paddingW">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <div class="bdBox bgMainColor topEgaoBox" data-aos="flip-up">
                            <div class="text-center mb30">
                                <p class="fontEn h1 mainColor">EGAORENSA</p>
                                <h3 class="white h1 bold titleIco">エガオレンサ IPA</h3>
                            </div>
                            <div class="white mb30">
<p>エガオレンサIPAは、ペールモルトとミュンヘンモルトにアメリカ産とオーストラリア産のホップをバランスよく組み合わせ、しっかりとした苦味を引き出したビールです。</p>
<p>フルーティーな飲み口から、濃厚で奥行きのあるフルボディの味わいまで、ゆっくりとご堪能ください。</p>
<p>日本国内でクラフトビールを醸造する400を超えるブルワリーから、麦酒島が厳選し、初めてプロデュースした特別な一杯です。</p>
<p>今夜の上陸の乾杯は、ぜひエガオレンサIPAで。</p>

</div>
                            <hr class="mb30">
                            <dl class="topEgaoDl dl-horizontal white clearfix">
                                <dt>品名</dt>
                                <dd>エガオレンサ IPA</dd>
                                <dt>ビアスタイル</dt>
                                <dd>ウエストコーストIPA</dd>
                                <dt>ABV(アルコール度数)</dt>
                                <dd>6.5%</dd>
                                <dt>IBU(国際苦味単位)</dt>
                                <dd>62</dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="" id="place">
    <div class="container">
        <div class="text-center mb50">
            <div class="topPlaceTitleWrap bgImg inlineBlock" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_shop_title_bg.png');" data-aos="zoom-in">
                <p class="fontEn h1 mainColor">PLACE TO DRINK</p>
                <h3 class="white h1 bold">飲めるところ</h3>
            </div>
        </div>
        <p class="white text-center mb80">エガオレンサIPAは麦酒島のオリジナルビールです。<br>岡山のクラフトビールのバー・麦酒島までお越し下さい。</p>
        <div class="row mb30">
            <div class="col-sm-6" data-aos="zoom-in-right">
                <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/top_place_01.jpg" alt="ビアアイランド">
            </div>
            <div class="col-sm-6" data-aos="zoom-in-left">
                <dl class="topEgaoDl topPlaceDl dl-horizontal white clearfix">
                    <dt>店名</dt>
                    <dd>樽生クラフトビールのお店<br>麦酒島－Beer Island－</dd>
                    <dt>住所</dt>
                    <dd>〒700-0825<br>岡山県岡山市北区田町2-7-4 荒尾ビル3F</dd>
                    <dt>電話番号</dt>
                    <dd>070-5045-0696</dd>
                    <dt>営業時間</dt>
                    <dd>18:00~26:00 (L.O.25:30)</dd>
                    <dt>休業日</dt>
                    <dd>日曜日</dd>
                    <dt>最寄り駅</dt>
                    <dd>岡山電気軌道清輝橋線 田町電停 徒歩5分<br>岡山電気軌道清輝橋線 新西大寺町筋電停 徒歩5分</dd>
                </dl>
            
            </div>
        </div>
        <ul class="topPlaceFlexUl flex justCenter mb30">
            <li>
                <a href="https://beer-island.com/" target="_blank" class="button h3 fontEn tra text-center mb30">WEB SITE</a>
            </li>
            <li>
                <a href="<?php echo home_url();?>/contact" class="button h3 fontEn tra text-center mb30">CONTACT</a>
            </li>
        </ul>
        
        <div class="mb50">
            <div class="text-center">
                <div class="text-center mb30">
                    <p class="fontEn h1 mainColor">Reservation</p>
                    <h4 class="white h1 bold titleIco">予約をする</h4>
                </div>
                <p class="white mb30">お電話でも下記のフォームでもご予約が可能です。</p>
            </div>

            <div class="footerhpReserve mb30">
                <script class="HPG_reserveScript" data-hpg-hpds-style="{'type': 'horizontal', 'color': 'white'}" data-hpg-hpds-jcode="{'J001224316': 'null'}" charset="UTF-8" type="text/javascript" src="https://www.hotpepper.jp/doc/hpds/js/hpds_variable_pc.js"></script>
            </div>
            <a class="telLink fontNum h1 text-center bold block mb0 white" href="tel:07050450696">070-5045-0696</a>
            <p class="text-center white">18:00 〜 26:00(日曜日除く)</p>
        </div>


<style>
.footerhpReserve .HPG_reserveGadgetsWrap{
	margin:0 auto !important;
}
</style>        
        
        <div class="map mb50">
        
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5575.1331695355775!2d133.91865490093252!3d34.65983745594831!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x355407b547f23c57%3A0x8336a30be7a7aa0e!2z5qi955Sf44Kv44Op44OV44OI44OT44O844Or44Gu44GK5bqXIOm6pumFkuWzti1CZWVyIElzbGFuZC0!5e0!3m2!1sja!2sjp!4v1574235614976!5m2!1sja!2sjp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>        
        </div>
        
    </div>
</section>



</main>



<?php get_footer(); ?>