<?php get_header(); ?>

<main>

<section class="pageFv mb50">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-7" data-aos="fade-right">
				<img class="" src="<?php echo get_template_directory_uri();?>/img/page_menu_fv.jpg" alt="ビアアイランドのメニュー">
			</div>
			<div class="col-sm-5" data-aos="fade-up">
				<div class="textColorBox whiteBoxRightWide relative bgSubColor">
					<p class="fontEn h00 mainColor mb0 fontEnNegaMa">Our Menu</p>
					<h3 class="bold mainColor h4 mb30">Beer Island Menu</h3>
					<p class="mb30">As well as craft beer fresh from the barrel, we serve beer cocktails, soft drinks and food that complements the beer, available as set menus. </p>
				</div>
			</div>
		</div>
	</div>
</section>



<section class="topMenu margin">
	<div class="container">
		
		<?php get_template_part('parts/temp-menu'); ?>
		
	</div>
</section>



<section class="topMenu margin" id="beer">
	<div class="container">
		<div class="text-center">
			<div class="titleBd">
				<p class="fontEn h00 white mb0 fontEnNegaMa">Craft Beer</p>
				<h3 class="bold white h4 mb30">Craft Beer</h3>
			</div>
		</div>
		<img class="mb30" src="<?php echo get_template_directory_uri();?>/img/page_menu_beer_main.jpg" alt="クラフトビー">
		<p class="width780 white mb50">Enjoy craft beers poured with love by the owner – a qualified beer sommelier. There are over 10 seasonal craft beers that change every week! Beause they are “fresh from the barrel” you can really compare the flavours of a host of quality craft beers.</p>
		<div class="row">
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">Craft Beer Board – 5-glass taster set</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">1,800Yen</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">Nekonihiki / Isekadoya Beer (Mie) size S</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">800yen</p>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">BIG BANG IPA / Uchu Brewing (Yamanashi) size S</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">800Yen</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">Peach Weizen / Minoh Beer (Osaka) size S</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">750Yen</p>
					</li>
				</ul>
			</div>
		</div>
		<div class="row mb50">
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">PORTER / Voyager Brewing (Wakayama) size S</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">750Yen</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">Indian Ao Oni / Yo-Ho Brewing (Nagano) size S</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">650Yen</p>
					</li>
				</ul>
			</div>
		</div>		<!--
		<div class="row">
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">Craft Beer Board – 5-glass taster set</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">1,800Yen</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">COPPER / Voyager Brewing (Wakayama) size S</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">780yen</p>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">Indian Ao Oni / Yo-Ho Brewing (Nagano prefecture) size S</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">680Yen</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">White Princess / Ise Kadoya Brewery  (Mie prefecture) size S</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">680Yen</p>
					</li>
				</ul>
			</div>
		</div>
		<div class="row mb50">
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">BIG BANG IPA / Uchu Brewing (Yamanashi prefecture) size S</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">780Yen</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold white">Weizen Merc / Tsuyama Beer Tadohonke Brewery (Okayama) size S</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif white mb0">680Yen</p>
					</li>
				</ul>
			</div>
		</div>
		-->
        <p class="white text-center mb30">※These are just a few examples.</p>
		<a href="https://r.gnavi.co.jp/a9zdmcxm0000/menu3/" target="_blank" class="button fontEn bold white tra text-center mb30" style="max-width:650px;">More</a>
	</div>
</section>


<section class="topMenu padding bgMainColor" id="cocktail">
	<div class="container">
		<div class="text-center">
			<div class="titleBd">
				<p class="fontEn h00 mb0 fontEnNegaMa mainColor">Beer Cocktail</p>
				<h3 class="bold mainColor h4 mb30">Beer Cocktails &amp; Other Drinks</h3>
			</div>
		</div>
		<img class="mb30" src="<?php echo get_template_directory_uri();?>/img/page_menu_bc_main.jpg" alt="ビアカクテル・その他ドリンク">
		<p class="width780 mb50">Beer lovers... why not open your mind to something new and experience a beer cocktail? There are many beer cocktails at Beer Island to be enjoyed. We also have non-alcoholic cocktails and soft drinks.</p>
		<div class="row">
			<div class="col-sm-4">
				<div class="mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_menu_bc_01.jpg" alt="瀬戸キュンレモネードビア">
					<h4 class="h5 bold mainColor titleBdBottom">Setouchi Lemonade Beer</h4>
					<p class="fontEnSerif mainColor">700Yen</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_menu_bc_02.jpg" alt="空色アイランドビア">
					<h4 class="h5 bold mainColor titleBdBottom">Sky Blue Island Beer</h4>
					<p class="fontEnSerif mainColor">700Yen</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_menu_bc_03.jpg" alt="ファジレッドビア">
					<h4 class="h5 bold mainColor titleBdBottom">Fuzzy Red Beer</h4>
					<p class="fontEnSerif mainColor">700Yen</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold mainColor">Grown-up Shandy Gaff</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif mainColor mb0">680Yen</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold mainColor">Peach Beer</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif mainColor mb0">680Yen</p>
					</li>
				</ul>
			</div>
		</div>
		<div class="row mb50">
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold mainColor">Cassis Beer</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif mainColor mb0">680Yen</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="inline_block pageMenuUl mb10">
					<li>
						<h4 class="h5 bold mainColor">Momotaro Red-eye</h4>
					</li>
					<li><hr></li>
					<li>
						<p class="fontEnSerif mainColor mb0">700Yen</p>
					</li>
				</ul>
			</div>
		</div>
		<a href="https://r.gnavi.co.jp/a9zdmcxm0000/menu2/" target="_blank" class="button fontEn bold white tra text-center mb30" style="max-width:650px;">More</a>
	</div>
</section>


<section class="topMenu margin" id="food">
	<div class="container">
		<div class="text-center">
			<div class="titleBd">
				<p class="fontEn h00 white mb0 fontEnNegaMa">Food</p>
				<h3 class="bold white h4 mb30">Food</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_menu_fd_06.jpg" alt="大人のポテサラ">
					<h4 class="h5 bold white titleBdBottom">Adult potato salad</h4>
					<p class="fontEnSerif white">500Yen</p>
					<p class="white text_m white">Once you start you can’t stop. Potato salad for grown-ups that is truly irresistable.</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_menu_fd_05.jpg" alt="岡山野菜のシーザーサラダ">
					<h4 class="h5 bold white titleBdBottom">Ceasar salad with Okayama vegetables</h4>
					<p class="fontEnSerif white">600Yen</p>
					<p class="white text_m white">Also available as a bouquet salad perfect for celebrations and parties.</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_menu_fd_04.jpg" alt="2種類のグリルソーセージ">
					<h4 class="h5 bold white titleBdBottom">Two varieties of grilled sausage</h4>
					<p class="fontEnSerif white">750Yen</p>
					<p class="white text_m white">Two delicious sausages of varying types carefully selected by the owner as the perfect complement to our craft beers.</p>
				</div>
			</div>
		</div>
		<div class="row mb50">
			<div class="col-sm-4">
				<div class="mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_menu_fd_03.jpg" alt="牛すじと生卵の絶品焼きカレードリア">
					<h4 class="h5 bold white titleBdBottom">Curry Doria with beef tendon and raw egg</h4>
					<p class="fontEnSerif white">750Yen</p>
					<p class="white text_m white">This is an absolute essential, and our most popular food! Melt in the mouth raw egg inside.</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_menu_fd_02.jpg" alt="クラフトビールに漬けたちょっと贅沢な鶏のから揚げ">
					<h4 class="h5 bold white titleBdBottom">Luxurious chicken karaage marinaded in craft beer</h4>
					<p class="fontEnSerif white">850Yen</p>
					<p class="white text_m white">Chicken karaage luxuriously marinaded in craft beer. The meat is very soft and juicy. Once you start you definitely won’t be able to stop!</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_menu_fd_01.jpg" alt="フライドポテト">
					<h4 class="h5 bold white titleBdBottom">Fries</h4>
					<p class="fontEnSerif white">450Yen</p>
					<p class="white text_m white">The ultimate nibbles with beer. There is a good-value extra large portion available too!</p>
				</div>
			</div>
		</div>
		<a href="https://r.gnavi.co.jp/a9zdmcxm0000/menu1/" target="_blank" class="button fontEn bold white tra text-center mb30" style="max-width:650px;">More</a>
	</div>
</section>


<section class="topMenu padding bgMainColor" id="course">
	<div class="container">
		<div class="text-center">
			<div class="titleBd">
				<p class="fontEn h00 mb0 fontEnNegaMa mainColor">Course Meal</p>
				<h3 class="bold mainColor h4 mb30">Set menus</h3>
			</div>
		</div>
		<img class="mb30" src="<?php echo get_template_directory_uri();?>/img/page_menu_course_main.jpg" alt="コース">
		<p class="width780 mb50">For parties, dates, a girls night out, welcome parties, drinks dos, end of year parties, or juist about any of your venue hire needs. There are 3 party set menus at 3,500 yen, 4,500 yen or 5,500 yen.  Book in advance to avoid disappointment.</p>
		<a href="https://www.hotpepper.jp/strJ001224316/" target="_blank" class="button fontEn bold white tra text-center mb30" style="max-width:650px;">More</a>
	</div>
</section>



<section class="topAbout spBgBlock pageMenuService">
	<div class="WhiteBox">
		<div class="container">
			<div class="text-center">
				<div class="titleBd mainColor">
					<p class="fontEn h00 mb0 fontEnNegaMa">Use Cases</p>
					<h3 class="bold h4 mb30">We are flexible</h3>
				</div>
			</div>
			<p class="mb30 width780">At Beer Island, we can cater to your needs. It’s fine if you want to drink alone or with friends, hire out the whole venue if you want to let loose with friends. Also, please enquire about our bartending service for your events.</p>
			<div class="row padding">
				<div class="col-sm-7" data-aos="fade-right">
					<div class="WhiteBoxLeft relative">
						<img class="" src="<?php echo get_template_directory_uri();?>/img/pagepage_menu_01.jpg" alt="貸し切りのご利用">
					</div>
				</div>
				<div class="col-sm-5" data-aos="fade-left">
					<div class="textColorBox whiteBoxRight relative bgSubColor">
						<p class="fontEn h00 mainColor mb0 fontEnNegaMa">Reserved</p>
						<h3 class="bold mainColor h4 mb30">Venue hire</h3>
						<p class="mb30">Possible because of the compact size of the venue... why not hire the whole room! Drink all your want in 2 hours in 3 types – this offer is for 10 people and over (max. 17 seated). Recommended for birthdays, mum’s meetings or for private gigs.</p>
					</div>
				</div>
			</div>
			<div class="row padding pageMenuServiceCont">
				<div class="col-sm-6 col-sm-push-6" data-aos="fade-left">
					<div class="WhiteBoxLeft relative">
						<img class="" src="<?php echo get_template_directory_uri();?>/img/page_menu_catering.png" alt="出張ビアバーテンダーサービス">
					</div>
				</div>
				<div class="col-sm-6 col-sm-pull-6" data-aos="fade-right">
					<div class="textColorBox whiteBoxRight relative bgSubColor">
						<p class="fontEn h00 mainColor mb0 fontEnNegaMa">Catering</p>
						<h3 class="bold mainColor h4 mb30">Visiting barman service</h3>
						<p class="mb30">The owner himself as a qualified beer sommelier will come to you as a barman for your event - anything from BBQs, socials, reunions or corporate events. We will deliver fresh craft beer!</p>
					</div>
				</div>
			</div>
			<!--
			<div class="row padding">
				<div class="col-sm-7" data-aos="fade-right">
					<div class="WhiteBoxLeft relative">
						<img class="" src="<?php echo get_template_directory_uri();?>/img/page_menu_03.jpg" alt="">
					</div>
				</div>
				<div class="col-sm-5" data-aos="fade-left">
					<div class="textColorBox whiteBoxRight relative bgSubColor">
						<p class="fontEn h00 mainColor mb0 fontEnNegaMa">Shopping</p>
						<h3 class="bold mainColor h4 mb30">お酒の通販</h3>
						<p class="mb30">ＢＡＲの醍醐味であるカウンター。
マスターとの会話、隣の方との出会い、一人の時間を過ごす。
そんな時間が過ごせる場所。
地下一階には秘密の場所をご用意いたしました。
使い方はご想像次第。
知れば知るほど面白くなります。</p>
					</div>
				</div>
			</div>
			-->
		</div>
	</div>
</section>




<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>