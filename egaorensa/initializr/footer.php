
<style>/*
<section class="sectionPankuzu">
	<div class="container">
		<?php get_template_part( 'parts/breadcrumb' ); ?>				
	</div>
</section>
*/</style>


<footer class="text_m text-left bgMainColor white footerCont">
	<div class="container">
		<div class="text-center">
			<p class="">岡山でクラフトビールが飲めるバー ビアアイランド</p>
			<a href="<?php echo home_url();?>"><p class="logo logo_footer remove mb10"><?php the_title();?></p></a>
		
			<p class="mb50 grayClolor">〒700-0825<br>岡山県岡山市北区田町2-7-4 荒尾ビル3F<a target="_blank" href="https://goo.gl/maps/mXSJPm8R4o4FBYgr6"><i class="fa fa-map-marker"></i></a><br>
				TEL：070-5045-0696<br>
				</p>
		</div>
	</div>
			<div class="wrapper_copy">
		<div class="container">
			<p class="text-center text_ss">copyright© 2019 beerisland all rights reserved.</p>
		</div>
	</div>
</footer>


<?php wp_footer(); ?>
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>



<script>
AOS.init({
	placement : "bottom-top",
	duration: 1000
});
</script>
</body>
</html>
 