<?php get_header(); ?>

<main>
<section class="padding" id="">
    <div class="container" data-aos="fade-up">
        <div class="text-center mb30">
            <p class="fontEn h1 mainColor">CONFIRM</p>
            <h3 class="white h1 bold titleIco">確認画面</h3>
        </div>
    </div>
</section>

<section class="margin">
	<div class="container">
		<div class="">
			<div class="contInCont white" data-aos="fade-up">
				<div class="mb30 text-center width780">
					<p>こちらの内容でお間違いないでしょうか？</p>
					<p>問題なければ下記の送信ボタンを押して下さい。</p>
				</div>
				<a class="telLink fontNum h1 text-center bold block mb0" href="tel:07050450696">070-5045-0696</a>
                <p class="gray mb30 text-center">18:00 〜 26:00(日曜日除く)</p>
				<div class="contactForm" data-aos="fade-up"><?php echo do_shortcode('[mwform_formkey key="28"]'); ?></div>
			</div>
		</div>
	</div>
</section>


<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>