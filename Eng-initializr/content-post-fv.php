<?php
	$category = get_the_category();
	$cat_id   = $category[0]->cat_ID;
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
?>

<!-- カテゴリーIDを表示したい所に -->
<?php //echo $cat_id; ?>

<!-- カテゴリー名を表示したい所に -->
<?php //echo $cat_name; ?>

<!-- カテゴリースラッグを表示したい所に -->
<?php //echo $cat_slug; ?>

				<a href="<?php the_permalink();?>" class="">
								<?php if (has_post_thumbnail()): ?>
	<?php 
		// アイキャッチ画像のIDを取得
		$thumbnail_id = get_post_thumbnail_id(); 
		// mediumサイズの画像内容を取得（引数にmediumをセット）
		$eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
		$eye_img_s = wp_get_attachment_image_src( $thumbnail_id , 'thumb_size_s_false',false );
	?>
					<div class="topFv__img" style="background-image:url(<?php echo $eye_img_s[0];?>);">
	    <?php else: ?>
					<div class="topFv__img" style="background-image:url(<?php echo get_template_directory_uri();?>/img/thumb_sample.png);">
	    <?php endif; ?>								
	
						<div class="container">
							<div class="row">
								<div class="col-sm-12">
									<div class="topFv__textArea text-center white">
										<p class="topFv__cate <?php echo $cat_slug; ?>"><?php echo $cat_name; ?></p>
										<h3 class="serif bold h0 lh_l"><?php the_title();?></h3>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</a>





