		<div class="row mb50 tempMenu" data-aos="fade-up">
			<div class="col-sm-3">
				<div class="footerMenuBox bgImg tra tempMenuBox1">
					<div class="bgBlackSub tra">
						<a href="<?php echo home_url();?>/menu#beer" class="footerMenuBoxText">
							<div class="h2 bold white text-center fontEn">Craft Beer</div>
							<!--<p class="h5 bold white text-center">クラフトビール</p>-->
						</a>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="footerMenuBox bgImg tra tempMenuBox2">
					<div class="bgBlackSub tra">
						<a href="<?php echo home_url();?>/menu#cocktail" class="footerMenuBoxText">
							<div class="h2 bold white text-center fontEn">Beer Cocktail</div>
							<!--<p class="h5 bold white text-center">ビアカクテル</p>-->
						</a>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="footerMenuBox bgImg tra tempMenuBox3">
					<div class="bgBlackSub tra">
						<a href="<?php echo home_url();?>/menu#food" class="footerMenuBoxText">
							<div class="h2 bold white text-center fontEn">food</div>
							<!--<p class="h5 bold white text-center">料理</p>-->
						</a>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="footerMenuBox bgImg tra tempMenuBox4">
					<div class="bgBlackSub tra">
						<a href="<?php echo home_url();?>/menu#course" class="footerMenuBoxText">
							<div class="h2 bold white text-center fontEn">Course Meal</div>
							<!--<p class="h5 bold white text-center">コース</p>-->
						</a>
					</div>
				</div>
			</div>
		</div>
