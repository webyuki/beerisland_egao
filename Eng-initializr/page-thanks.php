<?php get_header(); ?>

<main>
<section class="pageHeader relative">
	<div class="bgGrad pageHeaderText relative">
		<p class="pageHeaderEn fontEnBrush white">Thanks</p>
		<h3 class="h2 bold white">送信完了</h3>
	</div>
	<div class="pageHeaderImgBox bgImg absolute" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_about_01.jpg')"></div>
	
</section>



<section class="margin">
	<div class="container">
		<div class="row">
			<div class="contInCont" data-aos="fade-up">
				<div class="mb30 text-center width780">
					<p>お問い合わせの送信が完了しました。</p>
					<p>担当者から折り返させてご連絡させて頂きますので、今しばらくお待ち下さい。</p>
				</div>
				<a class="telLink fontEn h0 text-center bold mainColor block mb30" href="tel:0862222222">086-222-2222</a>
				<div class="contactForm" data-aos="fade-up"><?php echo do_shortcode('[mwform_formkey key="30"]'); ?></div>
			</div>
		</div>
	</div>
</section>


<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>