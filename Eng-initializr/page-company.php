<?php get_header(); ?>
<main>

<section class="pageHeader margin bgMainColor">
	<div class="container">
		<div class="white">
			<h2 class="bold h3">ヒカリ消毒について</h2>
			<h3 class="titleHeader mincho subColor">会社概要</h3>
		</div>
	</div>
</section>

<section class="margin pageGreeting">
	<div class="container">
		<h3 class="bold h3 text-center mb10">代表あいさつ</h3>
		<div class="titleBd mb10"></div>
		<p class="fontEn h5 bold mainColor text-center mb30">Greeting</p>
		<div class="row" data-aos="fade-up">
			<div class="col-sm-6">
				<h4 class="h3 bold mainColor mb10">昭和43年の創業から<br class="pc">地域の皆様の暮らしのパートナーとして</h4>
				<p>古来より、日本の建造物には木材が多く使用され、シロアリをはじめとする害虫や害獣の被害に悩まされてきました。駆除や予防の技術が進んだ現代においても、高温多湿となる気候や、自然豊かな国土の影響により、被害を完全になくすことはできていません。一方で長年に渡る研究から、シロアリをはじめとする害虫や害獣から建物を守るには、予防が最も重要であり、発生してしまった被害に対しては、それぞれの特徴に合った駆除方法が大きな効果を発揮することがわかっています。</p>

<p>私どもヒカリ消毒は<span class="bold">昭和43年の創業以来、地域の皆さまの暮らしのパートナーとして、倉敷市児島を拠点に</span>営業を続けてまいりました。創業より長きに渡って培ってきた経験と、新しく生まれた技術を組み合わせ、<span class="bold">害虫の根本的な駆除と予防による建物の総合的な管理</span>を行っています。<span class="bold">虫の生態と、建物の構造から虫の弱点を見つけて駆除することを重視した施工</span>には、お客さまから高い評価をいただいております。</p>

<p>また、私どもは高品質なサービスのご提供とともに、お客さまとは施工だけにとどまらない絆を結び、地域と住まいを愛する仲間として、末長くおつきあいさせていただきたいと考えております。古くよりご愛顧いただいているお客さまにも、新たにご依頼いただいたお客さまにも、変わらぬ安心をお約束し、長く信頼していただけることを一番の喜びとしてこれからも日々つとめてまいります。</p>

			</div>
			<div class="col-sm-6">
				<img class="pageGreetingImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_company_owner.jpg" alt="">
				<p class="text_m ">ヒカリ消毒株式会社<br>代表取締役　片沼 良宏</p>
			</div>
		</div>
	</div>
</section>
	
<section class="margin pageDetail bgGreen">
	<div class="container">
		<h3 class="bold h3 text-center mb10">会社概要</h3>
		<div class="titleBd mb10"></div>
		<p class="fontEn h5 bold mainColor text-center mb30">Company</p>
		<div class="pageAboutCompanyUl with780 mb100" data-aos="fade-up">
			<ul>
				<li>会社名</li>
				<li>ヒカリ消毒株式会社</li>
			</ul>
			<ul>
				<li>所在地</li>
				<li>	本部：〒711-0907 倉敷市児島上の町1丁目4番1号<a target="_blank" href="https://goo.gl/maps/747WSGWZitM2"><i class="fa fa-map-marker"></i></a><br>
				（本社：〒703-8244 岡山市中区藤原西町1丁目1番9号）<a target="_blank" href="https://goo.gl/maps/niCcPxUTxTp"><i class="fa fa-map-marker"></i></a><br></li>
			</ul>
			<ul>
				<li>連絡先</li>
				<li>TEL　086-473-6620　　FAX　086-473-9800
</li>
			</ul>
			<ul>
				<li>営業時間</li>
				<li>9:00-17:00</li>
			</ul>
			<ul>
				<li>定休日</li>
				<li>日曜日、祝日</li>
			</ul>
			<ul>
				<li>代表取締役</li>
				<li>片沼 良宏</li>
			</ul>
			<ul>
				<li>設立</li>
				<li>昭和43年</li>
			</ul>
			<ul>
				<li>対応可能エリア</li>
				<li>岡山県内全域</li>
			</ul>
			<ul>
				<li>加盟団体</li>
				<li>日本ペストコントロール協会会員<br>岡山県ペストコントロール協会会員<br>建築物ねずみ昆虫等防除業登録</li>
			</ul>
			<ul>
				<li>事業内容</li>
				<li>建築物の害虫(シロアリ・キクイムシ等)の防除・駆除全般<br>
衛生害虫(ゴキブリ・ネズミ・ムカデ等)の防除・駆除全般<br>
ハチ駆除・蜂の巣撤去<br>
床下換気扇、湿気・カビ対策、床下工事全般</li>
			</ul>
			<ul>
				<li>備考</li>
				<li>駐車場あり</li>
			</ul>
		</div>
	</div>
</section>
<!--
<iframe class="company_map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3282.3909629575355!2d133.8868227154208!3d34.644828093715844!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x35540766d4f6622f%3A0x8cd4d3be6a4d713c!2z44CSNzAwLTA5Nzcg5bKh5bGx55yM5bKh5bGx5biC5YyX5Yy65ZWP5bGL55S677yR77yU4oiS77yR77yQ77yR!5e0!3m2!1sja!2sjp!4v1531036447018" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
-->



<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>