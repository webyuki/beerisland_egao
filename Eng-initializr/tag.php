<?php get_header(); ?>

<main>

<?php
	$category = get_the_category();
	$cat_id   = $category[0]->cat_ID;
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
?>

<!-- カテゴリーIDを表示したい所に -->
<?php //echo $cat_id; ?>

<!-- カテゴリー名を表示したい所に -->
<?php //echo $cat_name; ?>

<!-- カテゴリースラッグを表示したい所に -->
<?php //echo $cat_slug; ?>

<section class="pageHeader bgMainColor mb100">
	<div class="bgImg bgCircle paddingW imgNone" style="background-image:url('<?php echo get_template_directory_uri();?>/img/bg_circle.png')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="white mb30">
						<h3 class="h3"><?php single_tag_title(); ?></h3>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</section>



<section class="pageNews margin">
	<div class="container">
		<?php //get_template_part( 'parts/breadcrumb' ); ?>				
		<div class="row">
			<div class="col-sm-9">
				<?php
					while ( have_posts() ) : the_post();
						get_template_part('content-post'); 
					endwhile;
				?>
			</div>
			<div class="col-sm-3">
				<?php dynamic_sidebar(); ?>
			</div>
		</div>
		<?php get_template_part( 'parts/pagenation' ); ?>
	</div>
</section>

</main>




<main>
<?php
	$category = get_the_category();
	$cat_id   = $category[0]->cat_ID;
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
?>

<!-- カテゴリーIDを表示したい所に -->
<?php //echo $cat_id; ?>

<!-- カテゴリー名を表示したい所に -->
<?php //echo $cat_name; ?>

<!-- カテゴリースラッグを表示したい所に -->
<?php //echo $cat_slug; ?>

<section class="pageHeader bgCenter">
	<div class="pageHeaderCover">
		<div class="container">
			<div class="text-center white pageHeaderTitle">
				<h3 class="titleJp h3 titleBd">「<?php single_tag_title(); ?>」のタグの記事一覧</h3>
				<!--<p class="titleEn questrial"><?php echo $cat_slug; ?></p>-->
			</div>
		</div>
	</div>
</section>

<section class="pageNews">
	<div class="container">
		<?php get_template_part( 'parts/breadcrumb' ); ?>				
		<div class="row">
			<div class="col-sm-9">
				<?php
					while ( have_posts() ) : the_post();
						get_template_part('content-post'); 
					endwhile;
				?>
			</div>
			<div class="col-sm-3">
				<?php dynamic_sidebar(); ?>
			</div>
		</div>
		<?php get_template_part( 'parts/pagenation' ); ?>
	</div>
</section>

</main>


<?php get_footer(); ?>