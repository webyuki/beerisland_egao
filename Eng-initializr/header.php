<!doctype html>
<html>
<head>
<meta charset="utf-8">
<!--キャッシュクリア-->
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<!--キャッシュクリア終了-->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<?php echo home_url();?>/favicon.ico" />
<title><?php wp_title('',true); ?><?php if(wp_title('',false)) { ?> | <?php } ?><?php bloginfo('name'); ?></title>
<?php wp_head(); ?>
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri();?>/apple-touch-icon.png">
<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/modal.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/loaders.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Muli:900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Aladin" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/main.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css">

<script src="<?php echo get_template_directory_uri();?>/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri();?>/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
<script src="<?php echo get_template_directory_uri();?>/js/vendor/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/jquery.matchHeight-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/parallax/parallax.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/rellax/rellax.min.js"></script>

<script src="<?php echo get_template_directory_uri();?>/js/main.js"></script>
<!--フォントプラス-->
<script type="text/javascript" src="//webfont.fontplus.jp/accessor/script/fontplus.js?tUOPpkwcnxA%3D&box=DPC0QA6Ps9Q%3D&aa=1&ab=2&aa=1&ab=2" charset="utf-8"></script>
<script>
/*高さ合わせる*/
$(function(){
	$('.matchHeight').matchHeight();
});

</script>
<script type="text/javascript">
$(function() {
	$('.slick-box').slick({
		prevArrow: '<div class="slider-arrow slider-prev fa fa-angle-left"></div>',
		nextArrow: '<div class="slider-arrow slider-next fa fa-angle-right"></div>',
		autoplay: true,
		autoplaySpeed: 4000,
		fade: true,
		speed: 2000,
	});
});
</script>

</head>

<body <?php body_class();?>>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v3.2&appId=473330072837166&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<!--ローディングCSS-->
<div id="js-loader">
	<div class="loader-inner ball-pulse">
	  <div></div>
	  <div></div>
	  <div></div>
	</div>
</div>

<style>
#js-loader {
	display: flex;
    position: fixed;
    height: 100vh;
    width: 100vw;
    z-index: 999;
    background: #9a6b31;
    align-items: center;
    justify-content: center;
}
</style>

<script>
// ローディング画面をフェードインさせてページ遷移
$(function(){
    // リンクをクリックしたときの処理。外部リンクやページ内移動のスクロールリンクなどではフェードアウトさせたくないので少し条件を加えてる。
    $('a[href ^= "https://newstella.co.jp"]' + 'a[target != "_blank"]').click(function(){
        var url = $(this).attr('href'); // クリックされたリンクのURLを取得
        $('#js-loader').fadeIn(600);    // ローディング画面をフェードイン
        setTimeout(function(){ location.href = url; }, 800); // URLにリンクする
        return false;
    });
});
 
// ページのロードが終わった後の処理
$(window).load(function(){
  $('#js-loader').delay(300).fadeOut(400); //ローディング画面をフェードアウトさせることでメインコンテンツを表示
});
 
// ページのロードが終わらなくても10秒たったら強制的に処理を実行
$(function(){ setTimeout('stopload()', 10000); });
function stopload(){
  $('#js-loader').delay(300).fadeOut(400); //ローディング画面をフェードアウトさせることでメインコンテンツを表示
}

</script>






<header>
	<div class="container">
		<div class="headerTop flex alignStart justBetween">
			<div class="headerLeft">
				<div class="headerText"><h2 class="text_s">The premiere craft beer bar in Okayama</h2></div>
				<h1 class="logo remove"><a href="<?php echo home_url();?>"><?php wp_title('',true); ?><?php if(wp_title('',false)) { ?> | <?php } ?><?php bloginfo('name'); ?></a></h1>
			</div>
			<div class="headerMenu pc text-center bold">
				<?php wp_nav_menu( array( 'menu_class' => 'flex justEnd alignCenter titleJp' ) ); ?>
			</div>
		</div>
	</div>
</header>

<!-- 開閉用ボタン -->

<div class="menu-btn sp" id="js__btn">
    <!--<span data-txt-menu="MENU" data-txt-close="CLOSE"></span>-->
	<div class="menu-trigger" href="#">
		<span></span>
		<span></span>
		<span></span>
		<span class="headerHunbergerText">MENU</span>
	</div>	
</div>

<!-- モーダルメニュー -->
<nav class="menu sp" id="js__nav">
	<?php wp_nav_menu( array( 'menu_class' => '' ) ); ?>
</nav>



<!--
<section class="contactSide absolute pc">
	<a href="<?php echo home_url();?>/contact" class="contactSideWap text-center">
		<div class="fontEn mainColor h5 titleBd">mail</div>
		<div class="titleJp mColor text_s">メール<span class="">は<br>こちらから</span></div>
	</a>
	
</section>
-->


<section class="footerTelSp sp">
	<ul class="inline_block">
		<li class="footerTelSpLi1 white text-center">
			<div class="text_ss">Reservation</div>
			<div class="footerTelSpLi1Tel h3 bold">
				<a href="tel:07050450696"><i class="fa fa-phone" aria-hidden="true"></i><span>070-5045-0696</span></a>
			</div>
		</li >
		<li class="footerTelSpLi2 text_m mainColor"><a href="tel:07050450696">Contract</a></li>
	</ul>
	
</section>

<script>
$(function () {
    var $body = $('body');

    //開閉用ボタンをクリックでクラスの切替え
    $('#js__btn').on('click', function () {
        $body.toggleClass('open');
    });

    //メニュー名以外の部分をクリックで閉じる
    $('#js__nav').on('click', function () {
        $body.removeClass('open');
    });
});

</script>

<?php
global $current_blog;
$blog_id = $current_blog->blog_id;
if ($blog_id == 1){ ?>
<div class="transBox">
	<ul class="transBoxUl inline_block white text_m fontEnSerif">
		<li class="current">JP</li>
		<li><a href="https://beer-island.com/en/">EN</a></li>
	</ul>
</div>

<?php } else if ($blog_id == 2){ ?>
<div class="transBox">
	<ul class="transBoxUl inline_block white text_m fontEnSerif">
		<li><a href="https://beer-island.com/">JP</a></li>
		<li class="current">EN</li>
	</ul>
</div>

<?php } ?>




 <!--<div id="google_translate_element"></div>-->
 

<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'ja', includedLanguages: 'en,ja', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
