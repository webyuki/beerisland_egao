<section class="sectionPankuzu">
	<div class="container">
		<?php get_template_part( 'parts/breadcrumb' ); ?>				
	</div>
</section>


<section class="footerMenu">
	<div class="container mb30">
		<div class="row">
			<div class="col-sm-4" data-aos="fade-up" data-aos-delay="0">
				<div class="footerMenuBox bgImg tra footerMenuBox1">
					<div class="bgBlack tra">
						<a href="<?php echo home_url();?>/about-us" class="footerMenuBoxText">
							<div class="h2 bold white text-center fontEn">About Us</div>
							<!--<p class="h5 bold white text-center">ビアアイランドについて</p>-->
						</a>
					</div>
				</div>
			</div>
			<div class="col-sm-4" data-aos="fade-up" data-aos-delay="200">
				<div class="footerMenuBox bgImg tra footerMenuBox2">
					<div class="bgBlack tra">
						<a href="<?php echo home_url();?>/menu" class="footerMenuBoxText">
							<div class="h2 bold white text-center fontEn">Menu</div>
							<!--<p class="h5 bold white text-center">メニュー</p>-->
						</a>
					</div>
				</div>
			</div>
			<div class="col-sm-4" data-aos="fade-up" data-aos-delay="400">
				<div class="footerMenuBox bgImg tra footerMenuBox3">
					<div class="bgBlack tra">
						<a href="<?php echo home_url();?>/news" class="footerMenuBoxText">
							<div class="h2 bold white text-center fontEn">News</div>
							<!--<p class="h5 bold white text-center">お知らせ</p>-->
						</a>
					</div>
				</div>
			</div>
		</div>
	
	
	</div>
</section>



<section class="footerInfor">
	<div class="container">
		<div class="row mb30">
			<div class="col-sm-6">
				<iframe class="company_map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13127.17448473954!2d133.9234402!3d34.6599146!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8336a30be7a7aa0e!2z6bqm6YWS5bO2LUJlZXIgSXNsYW5kLSDjg5PjgqLjgqLjgqTjg6njg7Pjg4kg77yc5bKh5bGx44O744Kv44Op44OV44OI44OT44O844Or44O744OT44O844Or44O76aOy44G_5pS-6aGM44O76LK45YiH44O75a605Lya44O7MuasoeS8mu-8ng!5e0!3m2!1sja!2sth!4v1542703187666" width="100%" height="471" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<div class="col-sm-6">
				<div class="footerInforBox bgMainColor textColorBox fontEnSerif">
					<div class="h0 mainColor mb10">
						<ul>
							<li class="footerTextBd"><a href="tel:07050450696">070-5045-0696</a></li>
							<li class="footerTextBd">18:00~26:00 <span class="h4">(L.O.25:30)</span></li>
							<li class="footerTextBd">Close > Sunday</li>
						</ul>
					</div>
					<div class="mainColor text_m">
						<p><span class="bold">Fresh craft beer shop - Beer Island</span><br>
						2nd Floor, Arao Building, 2-7-4 Tamachi, Kita-ku, Okayama City, Okayama Prefecture, 700-0825<a target="_blank" href="https://goo.gl/maps/sTcD6UkzU7u"><i class="fa fa-map-marker"></i></a><br>
5-minute walk from Okayama Electric Tramway, Seikibashi Line, Tamachi Tram Stop<br>
5-minute walk from Okayama Electric Tramway, Seikibashi Line, Shin-Saidaizi-cho-suzi Tram Stop</p>
						
					</div>
					<ul class="inline_block snnUl">
						<li><a href="https://web.facebook.com/beer.island.okayama/" target="_blank"><img src="<?php echo get_template_directory_uri();?>/img/ico_fb.png" alt=""></a></li>
						<li><a href="https://www.instagram.com/beer.island.okayama/" target="_blank"><img src="<?php echo get_template_directory_uri();?>/img/ico_insta.png" alt=""></a></li>
						<!--<li><a href=""><img src="<?php echo get_template_directory_uri();?>/img/ico_twitter.png" alt=""></a></li>-->
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

        
		        
		
		                
		                

<div class="wrapper_copy white">
	<div class="container">
		<p class="text-center text_ss">copyright© 2018 Beerisland all rights reserved.</p>
	</div>
</div>
<?php wp_footer(); ?>
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>



<script>
AOS.init({
	placement	: "bottom-top",
	duration: 1000,
});
</script>
</body>
</html>
