<?php get_header(); ?>

<main>

<section class="pageFv">
	<div class="container">
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-7" data-aos="fade-right">
				<img class="" src="<?php echo get_template_directory_uri();?>/img/page_about_fv.jpg" alt="麦酒島 - ビアアイランド - について">
			</div>
			<div class="col-sm-5" data-aos="fade-left">
				<div class="textColorBox whiteBoxRightWide relative bgSubColor">
					<p class="fontEn h00 mainColor mb0 fontEnNegaMa">About Us</p>
					<h3 class="bold mainColor h4 mb30"> About Beer Island</h3>
					<p class="mb30">Snug hideout-style bar and diner“Beer Island”. You can enjoy craft beers fresh from the barrel, chosen carefully by the owner – a qualified beer sommelier.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="topMenu padding">
	<div class="container">
		<div class="text-center">
			<div class="titleBd">
				<p class="fontEn h00 white mb0 fontEnNegaMa">Thought</p>
				<h3 class="bold white h4">About craft beer</h3>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row padding">
			<div class="col-sm-6 col-sm-push-6" data-aos="fade-left">
				<img class="mb30" src="<?php echo get_template_directory_uri();?>/img/page_about_01.jpg" alt="本物の樽生クラフトビールを">
			</div>
			<div class="col-sm-6 col-sm-pull-6" data-aos="fade-right">
				<div class="fluidTextBox white">
					<h4 class="bold h3 mb30">“The real craft beer fresh from the barrel”</h4>
					<p class="mb30">I wanted to pass on the delight and surprise I felt when I first tasted craft beer, that’s why I established this business in my home town - Okayama.</p>
					<p class="mb30">As with Japanese sake, shochu, wine and whiskey, there is depth of flavour in beer. There are currently more than 300 breweries in Japan producing craft beers. We present an expertly selected selection of beers from across the country unique to the local regions carefully by the owner – a qualified beer sommelier.</p>
					<p class="mb30">Freshness and good care are essential for maintaining quality beer. No matter how delicious a beer starts out, if it’s not properly cared for the full and delicate range of flavour is lost. We constantly use our expertise to monitor our beers to nurture and preserve 100% of the flavour. You have to taste it to experience‘fresh from the barrel’ quality.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6" data-aos="fade-right">
				<img class="" src="<?php echo get_template_directory_uri();?>/img/page_about_02.jpg" alt="小萩 倫久">
			</div>
			<div class="col-sm-6" data-aos="fade-left">
				<div class="fluidTextBoxRight white">
					<div class="mb30"></div>
					<p class="text_m fontEnSerif mb0">The Owner</p>
					<p class="bold h4 titleBdBottom mb30"> <span class="text_m gray fontEnSerif">Kohagi Michihisa</span></p>
					<p class="text_m gray">Worked as a musician for around 10 years in Osaka. A beer lover, who was seduced by the appeal of craft beers whilst acquiring the qualification as a beer sommelier. Beer Island was established to make many people smile through the delight of craft beer. The owner also promotes beer at pop-up exhibitions and beer festivals as well as hosting events.</p>
				</div>
			</div>
		</div>
	</div>
</section>





<section class="topAbout pageMenuService spBgBlock">
	<div class="WhiteBox">
		<div class="container">
			<div class="text-center">
				<div class="titleBd mainColor">
					<p class="fontEn h00 mb0 fontEnNegaMa">Features</p>
					<h3 class="bold h4 mb30">The appeal of Beer Island</h3>
				</div>
			</div>
			<p class="width780">At Beer Island you can spend time chatting to the owner, meeting the person sitting next to you and generally enjoying your time alone or with friends. 
“It’s up to you how you spend your time in the hideout. It’ll only get more enjoyable as your appreciation of craft beer grows.”</p>
			<div class="row padding">
				<div class="col-sm-6" data-aos="fade-right">
					<div class="WhiteBoxLeft relative">
						<img class="" src="<?php echo get_template_directory_uri();?>/img/page_menu_01.jpg" alt="隠れ家的ダイニングバーで楽しい時間を">
					</div>
				</div>
				<div class="col-sm-6" data-aos="fade-left">
					<div class="textColorBox whiteBoxRight relative bgSubColor">
						<p class="fontEn h00 mainColor mb0 fontEnNegaMa">Dining Bar</p>
						<h3 class="bold mainColor h4 mb30">“Have a great time at this cosy hideout-style bar and diner”</h3>
						<p class="mb30">Beer Island’s hideout-style bar and diner is exclusive and relaxing, right here in Tamachi, Kitaku, Okayama City. You can relax and enjoy craft beers to your heart’s content, just chilling out and whiling away the time.</p>
					</div>
				</div>
			</div>
			<div class="row padding pageMenuServiceCont">
				<div class="col-sm-6 col-sm-push-6" data-aos="fade-left">
					<div class="WhiteBoxLeft relative">
						<img class="" src="<?php echo get_template_directory_uri();?>/img/page_menu_02.jpg" alt="ビアソムリエが厳選したクラフトビールやフードを堪能">
					</div>
				</div>
				<div class="col-sm-6 col-sm-pull-6" data-aos="fade-right">
					<div class="textColorBox whiteBoxRight relative bgSubColor">
						<p class="fontEn h00 mainColor mb0 fontEnNegaMa">Beer Sommelier</p>
						<h3 class="bold mainColor h4 mb30">“Enjoy craft beers and food carefully selected by our qualified beer sommelier”</h3>
						<p class="mb30">Every week our lineup of seasonal craft beers is updated, all 100% fresh from the barrel. There is always a new beer to try, no matter when you pop in. Food is chosen to complement the beers with the expertise of a beer sommelier, maximising your beer enjoyment.</p>
					</div>
				</div>
			</div>
			<div class="row padding">
				<div class="col-sm-7" data-aos="fade-right">
					<div class="WhiteBoxLeft relative">
						<img class="" src="<?php echo get_template_directory_uri();?>/img/page_menu_03.jpg" alt="店舗貸切やパーティーコースも">
					</div>
				</div>
				<div class="col-sm-5" data-aos="fade-left">
					<div class="textColorBox whiteBoxRight relative bgSubColor">
						<p class="fontEn h00 mainColor mb0 fontEnNegaMa">Party Course</p>
						<h3 class="bold mainColor h4 mb30">“Hire the bar or choose party set menus”</h3>
						<p class="mb30">You can hire out the venue with only around 10 people because of our compact size, allowing us to tailor your party experience to your requirements. On our party menu we offer drink-all-you-can craft beer, which is unusual in Okayama. Beer Island is perfect as your main party venue.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="">
	<div class="WhiteBox">
		<div class="container">
			<div class="text-center">
				<div class="titleBd mainColor">
					<p class="fontEn h00 mb0 fontEnNegaMa">Event & Voice</p>
					<h3 class="bold h4 mb30">過去に行ったイベント・お客さまの声</h3>
				</div>
			</div>
			<div class="row mb50" data-aos="fade-up">
			
				<?php
					//$paged = (get_query_var('page')) ? get_query_var('page') : 1;
					$paged = get_query_var('page');
					$args = array(
						'post_type' =>  'post', // 投稿タイプを指定
						'paged' => $paged,
						'posts_per_page' => 4, // 表示するページ数
						'orderby'=>'date',
						'cat' => 4,
						'order'=>'DESC'
								);
					$wp_query = new WP_Query( $args ); // クエリの指定 	
					while ( $wp_query->have_posts() ) : $wp_query->the_post();
						//ここに表示するタイトルやコンテンツなどを指定 
					get_template_part('content-post-top'); 
					endwhile;
					//wp_reset_postdata(); //忘れずにリセットする必要がある
					wp_reset_query();
				?>		
			
			</div>
			<a href="<?php echo home_url();?>/category/event/" class="button fontEn bold ml0 white tra text-center h4">MORE</a>
			
		</div>
	</div>
</section>



<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>